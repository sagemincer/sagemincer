Mako==1.0.0
MarkupSafe==0.23
SPARQLWrapper==1.6.4
bottle==0.12.7
html5lib==0.999
isodate==0.5.0
lxml==4.1.1
mimerender==0.5.4
mock==1.0.1
pyparsing==2.0.2
python-mimeparse==0.1.4
pyxdg==0.25
rdflib==4.1.2
six==1.7.3
wsgiref==0.1.2
# We require FuXi from GitHub because it supports RDFLib 4
-e git+https://github.com/RDFLib/FuXi.git@d4342c74036c0d26e03d11855f4a51dd0152c5cf#egg=FuXi
