Broadly there are two things that need to be done by sagemincer:

1. Fetch information about security rules from authoritative sources, and
   digest it.
2. Provide the information to those who request it.

The first function must happen periodically, because the authoritative sources
provide updates to their security rules.

The second function is going to happen with a web application, because we're
going to provide the information over the web using the Linked Open Data
paradigm.



Fetching information
====================

Authoritative sources
---------------------

1. DISA's quarterly STIG library compilation.
2. More to come.


DISA's quarterly STIG library compilation
-----------------------------------------

This is a ~200MB zip file that contains many other zip files that contain other
zip files that contain XCCDF documents. The URL does not change, but the file
at that URL does, every quarter. The server from which we get the file returns
an HTTP ETag header when we make a HEAD or GET request for the file. The script
that checks, fetches and processes this thing may be limited in how long it can
run. (On OpenShift Online, it is.)

So several possibilities exist:

1. The file could have changed on the server since we looked last.
2. We may not have enough time to download the whole thing in one cron run.
3. We may not have enough time to digest the whole thing in one cron run.
4. Most of the STIGs inside a new file will be the same as those inside an old
   file, and we don't want duplicate data to be stored.
5. Some of the STIGs inside a given zip file may be duplicates.
6. Some STIGs inside a given zip file may have the same filename but contain
   different content.


DISA's XCCDF content
--------------------

DISA STIGs have interesting variations against what you might expect just from
reading the XCCDF standard. It's important to gather the information DISA
provides, even if it isn't in a place or form that the XCCDF standard dictates.

DISA STIGs are made with XCCDF 1.1 or later. Each thing inside a STIG must have
a unique ID, expressed in its id element. Based on some legwork done in early
2014, all observed Group, Rule and Profile IDs in hundreds of STIGs were valid
`NCName`_ s; several dozen Benchmark IDs were not valid NCNames.

.. _`NCName`: http://www.w3.org/TR/xmlschema-2/#NCName

Providing information
=====================

Provide security rule information via HTML to people with web browsers. Make it
look nice.

Provide security rule information via Turtle and RDF/XML to scripts.

URIs
----

Given the use of the Linked Open Data paradigm, we'll be identifying security
checklists, security requirements, and other things with URIs. An identifier
used to make URIs needs to be reliably unique, easily derived, automatically
constructed, an `NCName`_, and short. It should differ for different versions
of a thing, and for different things. It's much less of a problem if it differs
when it shouldn't differ than if it's the same when it shouldn't be: upon
inspection we can find that two things with different names are in fact the
same, and notate this fact, but if two different things have the same name, we
can't refer to or even reliably find both things.

