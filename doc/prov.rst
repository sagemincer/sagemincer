Flows:
New content pushes to old content depending on it. This is like seeing.
Old content needs to be pulled in to mix with the new content. This is like remembering.
Collect all new content first to minimize the number of epiphanies.


Find new content
----------------

Downloading:
Input is a list of URLs to zip files.
For each URL:
  If we've downloaded this one before, conditionally GET using the ETag and modified date.
    If 304 Not Modified, continue.
    If 200 OK, download and pass on, with ETag and modified date.
  If not, unconditionally GET.
  Save the data into a place on the filesystem.
  Take the sha256.
  Pass on the local filename and checksum, with ETag and modified date.

Examining axioms: (these are XSLT transforms and rulish RDFS/OWL inputs)
For each file:
  Get the contents using pkg_resources.
  Take the sha256.
  Pass on the checksum.

Examining the version of sagemincer:
Take some version number, and pass on facts about it if it is new.
(The version of sagemincer may not be the right number to use here: we
may need a separate number to express when changes are made that
influence how the data is interpreted)

Figure out when newly downloaded files supersede old ones. Sometimes
these will be different resources with the same URL (replaced files),
sometimes new files.

Save all metadata into the triplestore. Now all files, old and new,
can be identified and located.


Reassess
--------

Find all graphs that depend on files that have been
superseded. Refigure. For some changed dependencies, old results
should be kept, but not for others, viz., stylesheets and rulish
graphs will stay backward compatible, but documents from outside may
not. 

A checklist may not make sense if interpreted in
light of a CCI or 800-53 list newer than the checklist. XSLT
transforms newer than checklists will of course happen all the time,
and the burden of backward compatibility is borne here in sagemincer.
