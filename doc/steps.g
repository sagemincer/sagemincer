digraph g {
# Blue nodes are entities.
    quarterly_release [color=blue];
# STIG zips are kept around indefinitely so they have a bold border.
    stig_zip [color=blue, style=bold];
    xccdf_text [color=blue];
    stylesheet1 [label="stylesheet", color=blue];
    stylesheet2 [label="stylesheet", color=blue];
    named_graph [color=blue];
    stig_zip_on_server [color=blue];
    quarterly_release_on_server [color=blue];
# Other nodes are activities.
    unzip1 [label="unzip"];
    unzip2 [label="unzip"];
    head1 [label="head"];
    head2 [label="head"];
    download1 [label="download"];
    download2 [label="download"];
    transform1 [label="transform"];
    transform2 [label="transform"];
# Gray edges mean that we find through an activity that an entity available
# from the server is the same as one we've already gotten.
# Multiple default edges mean multiple results of an activity.

    head1 -> quarterly_release_on_server [color=gray];
    head1 -> quarterly_release_on_server;
    quarterly_release_on_server -> download1;
    download1 -> quarterly_release;
    quarterly_release -> unzip1;
    unzip1 -> stig_zip;
    unzip1 -> stig_zip;
    check_stig_rss -> head2;
    check_stig_rss -> head2;
    head2 -> stig_zip_on_server [color=gray];
    head2 -> stig_zip_on_server;
    stig_zip_on_server -> download2;
    download2 -> stig_zip;
    stig_zip -> unzip2;
    unzip2 -> xccdf_text;
    unzip2 -> xccdf_text;
    xccdf_text -> transform1;
    stylesheet1 -> transform1;
    transform1 -> named_graph;
    xccdf_text -> transform2;
    stylesheet2 -> transform2;
    transform2 -> named_graph;
    named_graph -> store_graph;
}
