Six-mile view
=============

sagemincer consists of a fetcher and a web application, both backed by an RDF
triplestore.

The fetcher downloads files containing security rule content in XML formats. It
uses XSLT stylesheets to transform these into RDF/XML, and inserts them into
the triplestore.

The web application, made with `Bottle <http://bottlepy.org>`_, pulls data out
of the triplestore, and presents it in a document of the MIME type requested.


RDF
===

RDF frees the data from the document. Each thing we're talking about, not only
the document containing or describing the thing, gets a URI. That fulfills the
requirement of letting someone refer to a single security requirement from
anywhere.

Its data model is like unto that of XML, so it's easy and natural to mince an
XML document into RDF triples.

Much more might be said generally about the merits of RDF, but has already been
said by others.


The triplestore
===============

rdflib can deal with graphs containing tens of thousands of triples. But when
processing hundreds of thousands of triples, it takes more RAM than may be
depended upon without paying extra money for hosting; and when storing hundreds
of thousands of triples, its Berkeley DB datastore fails mysteriously.

Triplestores, the RDF analogue of database servers, are built for scale. 4store
is a triplestore which is Free Software. The availability of the source, and
ability to build it in ways Garlik did not imagine, made it possible to put
4store on OpenShift, and so to publicly host sagemincer with ease and
frugality.


The fetcher
===========

The fetcher constructs some Download objects, one for each file that should
possibly be downloaded. The Download knows what ETags of this file we've fully
processed before. It holds a LengthyDownloadManager, which it uses to possibly
download a new version of the file. The LDM figures out whether the file has
changed relative to what we've processed last, downloads the file, saves and
restores its progress so downloads can span multiple fetcher runs, and caches
the latest fetched version of the file. If the LDM fetches a new version of the
file, the Download takes care of finding all the XCCDF content in the file, and
creating a Checklist object for each XCCDF document found.

Checklist objects calculate their own IDs, using a checksum over the contents
of the XCCDF document. Based on this they can tell whether the data in the
document already exists in the triplestore. Checklist objects also do the XSLT
transform that turns the XCCDF into RDF/XML suitable for uploading to the
triplestore.


The web application
===================

The web application answers HTTP requests for checklists, groups, items, etc.
It's made using Bottle, and it's specific to the purpose of showing information
about security rules. (rdflib_web was considered---it's much more general and
already made---but it couldn't serve information about the URIs in our
triplestore, because they differ in more than just their last path element or
fragment.)


URI design
==========

Being that our data storage is based on RDF, and RDF facts are triples of two
URIs and a literal, or three URIs, the design of the URIs in this application
is important. The sagemincer module (sagemincer/__init__.py) contains rdflib
namespace objects used in constructing the URIs. Here are the designs.

Vocabulary
----------

An example URI from the vocabulary is
<http://xccdflod.agrue.info/ns/xccdflod/1#Group>. The part before the ``#`` is
the namespace of the XCCDFLOD vocabulary. The ns path element is there to
distinguish the URI for the namespace from the URIs for other things, like
pages or security requirements. The ``xccdflod`` is there because we will end
up with multiple vocabularies and/or ontologies. Small orthogonal vocabularies
are likely better than a single large one with loosely related concepts. The
``1`` denotes the major version of the vocabulary. Backward-incompatible
changes to the vocabulary will result in this number being incremented, and
backwards-compatibility measures will be taken, such as figuring out a mapping
between the concepts in versions 1 and 2 and applying the mapping to triples in
the store in order to migrate to version 2.

An example URI naming a security requirement is
<http://xccdflod.agrue.info/id/cl/Ua5KR0FCeH/V-29492>. The ``id`` signifies
that this is a URI used for identifying something which isn't directly a
document. If this is confusing, see `Cool URIs for the Semantic Web`_. The
``cl`` means the thing identified first is a checklist. The ``Ua5KR0FCeH`` is a
truncated checksum of the original checklist content. We'll talk about why this
is the unique identifier shortly.  The ``V-29492`` is an identifier inside the
checklist document. This identifier identifies the Group element
GEN000000-AIX0090 in the AIX 5.3 STIG, Version 1, Release 2.

.. _`Cool URIs for the Semantic Web`: http://www.w3.org/TR/cooluris

An example URI naming a `landing page` (defined in `Cool URIs for the Semantic
Web`_) is <http://xccdflod.agrue.info/about/cl/Ua5KR0FCeH/V-29492>. The
``about``, as opposed to ``id``, is what makes this so. This is a page `about`
the Group GEN000000-AIX0090 in the AIX 5.3 STIG, Version 1, Release 2. The rest
of the URI is as above.

Identifying checklists
----------------------

Now, why does ``Ua5KR0FCeH`` mean the AIX 5.3 STIG V1R2?

* The original filenames of the STIGs were not reliably unique across the set,
  some contained spaces, and most were too long.
* The id attributes of the Benchmark elements in the STIGs were not all
  NCNames, and most were too long.
* The versions indicated in the STIG content did not reliably differ when there
  were differences in the content.

So we just hash the content, base64 the hash, and truncate it for brevity.
