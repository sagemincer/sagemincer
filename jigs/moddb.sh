#!/bin/sh

SPARQL_UPDATE_URL=http://${OPENSHIFT_4S_IP:-localhost}:${OPENSHIFT_4S_PORT:-9000}/update/

# for spacing of help messages
Z=$(echo "$0" | sed 's/./ /g')

send_update_query () {
    escaped_query="$(echo $1 | sed -e 's/:/%3A/g; s/\?/%3F/g; s/\#/%23/g; s/ /+/g;' | perl -pe 's/\n//g;')"
    (set -x; curl -i -d "update=$escaped_query" $SPARQL_UPDATE_URL)
}

fetch () {
    python -m sagemincer.fetch
}


_help_q_undownload () {
    cat >&2 <<EOF
    $0 undownload         - delete records of successful downloads so entire
    $Z                      files will not be skipped in the fetch
EOF
}
_q_undownload () {
    send_update_query '
       DELETE { ?s <http://securityrules.info/ns/xccdflod/1#downloadETag> ?o } 
       WHERE { ?s <http://securityrules.info/ns/xccdflod/1#downloadETag> ?o }'
}


_help_q_unknow () {
    cat >&2 <<EOF
    $0 unknow             - delete records of successful document ingests so
    $Z                      individual files will not be skipped in the fetch
EOF
}
_q_unknow () {
    send_update_query '
        DELETE { ?s <http://securityrules.info/ns/xccdflod/1#sha256is> ?o }
        WHERE { ?s <http://securityrules.info/ns/xccdflod/1#sha256is> ?o }'
}


_help_q_delete_ccis () {
    cat >&2 <<EOF
    $0 delete_ccis        - delete all CCI lists
EOF
}
_q_delete_ccis () {
    send_update_query '
        DELETE { GRAPH ?g { ?s ?p ?o } }
        WHERE { ?g a <http://securityrules.info/ns/ccilod/1#CCIList> }'
}


_help_q_reread_cci () {
    cat >&2 <<EOF
    $0 reread_cci         - delete CCIs, undownload and unknow; then run fetch
EOF
}
_q_reread_cci () {
    _q_delete_ccis
    _q_undownload
    _q_unknow
    fetch
}


usage () {
    echo "usage:" >&2
    declare -F | while read dontcare1 dontcare2 funcname; do
	if [[ $funcname = _q_* ]]; then
	    _help$funcname
	fi
    done
    exit 1
}
	
if [ $# = 1 ]; then
    funcname="_q_$1"
    if [ "$(type -t $funcname)" = "function" ]; then
	shift
	$funcname "$@"
    else
	usage
    fi
else
    usage
fi

