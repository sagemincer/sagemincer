#!/bin/sh

sudo service 4store stop
sudo 4s-backend-setup --node 0 --cluster 1 default
sudo chown -R fourstore /var/lib/4store
sudo service 4store start
