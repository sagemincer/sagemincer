// vim: sw=4 sts=4 et ai si mouse=a
// https://stackoverflow.com/questions/15739263/phantomjs-click-an-element
var page = require('webpage').create();

function hasNextLink() {
    var nextImages = document.querySelectorAll(
            'td#bottomPagingCellWPQ4 img[alt="Next"]');
    if(nextImages.length > 0) {
        var a = nextImages[0].parentElement;
        if(a) {
            return true;
        }
    }
    return false;
}

function clickNextLink() {
    var nextImages = document.querySelectorAll(
            'td#bottomPagingCellWPQ4 img[alt="Next"]');
    if(nextImages.length > 0) {
        var a = nextImages[0].parentElement;
        if(a) {
            a.onclick();
        }
    }
}

function zipsInTable() {
    var as = document.querySelectorAll('table.ms-listviewtable a[href]');
    var thezips = [];
    for(var i = 0; i < as.length; i++) {
        if(as[i].href.match(/\.zip$/i)) {
            thezips.push(as[i].href);
        }
    }
    return thezips;
}

function main() {
    var zips = [];
    var screenshotNumber = 0;
    var anyMorePages = true;
    function scrapeThisPage() {
        zips = zips.concat(page.evaluate(zipsInTable));
        if(page.evaluate(hasNextLink)) {
            page.evaluate(clickNextLink);
        } else {
            for(var i = 0; i < zips.length; i++) {
                console.log(zips[i]);
            }
            phantom.exit();
        }
    }
    page.onLoadFinished = scrapeThisPage;
    page.open('http://iase.disa.mil/stigs/Pages/a-z.aspx');
}

main();
