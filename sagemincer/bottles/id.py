# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bottle import Bottle, request, HTTPResponse, HTTPError, SimpleTemplate
import re
from base64 import urlsafe_b64decode
from sagemincer import to_current_hash_format
from rdflib import Namespace

# See <http://www.w3.org/TR/urls-in-data/>.
# The data we provide in this application is all on landing pages or
# records, as defined. So our identifiers all redirect to the landing
# pages, indicating the descriptive relationship between the identifier
# URIs and the landing page URIs.
#
# See <http://www.w3.org/TR/cooluris/#choosing> on 303 URIs.

app = Bottle()

def _redirect_to_landing_page(landing_page):
    return HTTPResponse(status=303, headers={
            'Link': '<{}>; rel="describes"'.format(landing_page),
            'Location': landing_page,
            })

def _redirect_to_id(id_uri):
    return HTTPResponse(status=302, headers={
            'Location': id_uri,
            })

# These are a bit prosaic, but the idea is to constrain the possibilities to
# what we expect, in the name of security.

@app.route('/<doc_hash>')
@app.route('/<doc_hash>/')
@app.route('/<doc_hash>/top')
def document(doc_hash):
    landing_page = '/about/{}'.format(to_current_hash_format(doc_hash))
    return _redirect_to_landing_page(landing_page)

@app.route('/<doc_hash>/<item_id>')
def item(doc_hash, item_id):
    landing_page = '/about/{}/{}'.format(to_current_hash_format(doc_hash), item_id)
    return _redirect_to_landing_page(landing_page)
