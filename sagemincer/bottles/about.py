# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bottle import Bottle, HTTPError, response
from sagemincer.bottles.common import RDFXMLGraphResponder, TurtleGraphResponder
from sagemincer.bottles.common import _fragment, get_classes_or_404
from sagemincer.bottles.common import add_static, template
from rdflib import RDFS, XSD, URIRef
from rdflib.namespace import Namespace
from sagemincer import (
    Xid, XCCDFLOD, CCILOD, SP80053LOD, STIGLOD, PROV)
import functools
import mimerender
import logging
import time
import pkg_resources
from sagemincer import raw_hash, safen_hash
from sagemincer.fastgraph import FastGraph, graph_as_turtle, graph_as_rdfxml
from sagemincer.fastgraph import item_as_turtle, item_as_rdfxml
from sagemincer.fs_sparql import SPARQLWrapper, JSON
from sagemincer.environment import sparql_url
import re
from collections import defaultdict
import datetime

with_mimerender = mimerender.BottleMimeRender()

app = Bottle()

# vvv request timing, from http://bottlepy.org/docs/0.12/plugindev.html
def stopwatch(callback):
    def wrapper(*args, **kwargs):
        log = logging.getLogger(__name__ + '.' + callback.func_name)
        start = time.time()
        body = callback(*args, **kwargs)
        end = time.time()
        log.debug('request served in %g sec', end - start)
        return body
    return wrapper
app.install(stopwatch)
# ^^^ request timing

add_static(app)


class EntireGraphRDFXML(RDFXMLGraphResponder):
    get_query_response = graph_as_rdfxml

class EntireGraphTurtle(TurtleGraphResponder):
    get_query_response = graph_as_turtle

class ItemRDFXML(RDFXMLGraphResponder):
    get_query_response = item_as_rdfxml

class ItemTurtle(TurtleGraphResponder):
    get_query_response = item_as_turtle

    
def _prepare_template_namespace(document_uri, document_classes, **kwargs):
    cache_predicates, only_consult_cache_predicates = set([]), set([])
    for c in document_classes:
        add_cache_predicates, add_only_consult_cache_predicates = document_html_fastgraph_bla[c]
        cache_predicates.update(set(add_cache_predicates))
        only_consult_cache_predicates.update(set(add_only_consult_cache_predicates))
    fastgraph = FastGraph(document_uri, cache_predicates, only_consult_cache_predicates)
    document_ns = Namespace(document_uri + '/')
    label_for = fastgraph.partial_getvalue(RDFS.label, 'unknown')
    lexnum_on_label = lambda thing: alternating_lexical_numerical(label_for(thing))
    template_namespace = {
        'top': document_ns.top,
        'label_for': label_for,
        'comment_for': fastgraph.partial_getvalue(RDFS.comment),
        'get_all': lambda s,p: sorted(fastgraph.objects(s,p), key=label_for),
        'all_with': lambda p,o: sorted(fastgraph.subjects(p,o), key=label_for),
        'get_one': fastgraph.getvalue,
        'get_all_lexnum': lambda s,p: sorted(fastgraph.objects(s,p),
                                             key=lexnum_on_label),
        'CCILOD': CCILOD,
        'XCCDFLOD': XCCDFLOD,
        'SP80053LOD': SP80053LOD,
        'STIGLOD': STIGLOD,
        'RDFS': RDFS,
        'fragment': _fragment,
        'get_url': app.get_url,
        'document_uri': document_uri,
    }
    template_namespace.update(**kwargs)
    return template_namespace

def alternating_lexical_numerical(k):
    m = re.match('(\D+)(\d+)?', k)
    if m:
        notdigits, digits = m.groups()
        number = int(digits) if digits is not None else digits
        return (notdigits, number) + alternating_lexical_numerical(k[m.end():])
    else:
        return ()

def last_modified(uri, sparql_url):
    """Find a Last-Modified date and time for a generated resource.

    "Generated" means that the resource prov:wasGeneratedBy some
    prov:Activity, which has a prov:endedAtTime.

    "Last-Modified" means suitable for use in a Last-Modified HTTP
    header, i.e. if we can't find a suitable date we say it was last
    modified now, and if the date found is in the future, we clamp it
    to now, to avoid deleterious effects on caching, as per
    <http://tools.ietf.org/html/rfc7232#section-2.2>.

    """
    w = SPARQLWrapper(sparql_url)
    w.setQuery('''\
        PREFIX prov: {prov_uri}
        SELECT ?et WHERE {{
            {uri} prov:wasGeneratedBy ?act .
            ?act prov:endedAtTime ?et .
        }}'''.format(
            prov_uri=URIRef(PROV).n3(), uri=uri.n3()))
    w.setReturnFormat(JSON)
    now = datetime.datetime.utcnow()
    for r in w.query().convert()['results']['bindings']:
        if r['et']['datatype'] == str(XSD.dateTime):
            modified = min(
                datetime.datetime.strptime(r['et']['value'],
                                           '%Y-%m-%dT%H:%M:%S.%f'),
                now)
            break
    else:
        modified = now
    return modified

def http_date(dt):
    """Convert a datetime in UTC to a string in the way HTTP specifies."""
    # BUG: RFC 7231 says an HTTP-date is as defined in RFC 5322. The
    # latter defines day names as Mon, Tue, Wed, etc., but %a gets us
    # the *localized* abbreviated day name, so if you run this in a
    # non-English locale, it will emit a non-compliant date string.
    # BUG: We ignore here the subtle differences between UTC and GMT.
    # and finally - thanks, http://stackoverflow.com/a/1472008.
    return dt.strftime('%a, %d %b %Y %H:%M:%S GMT')

def set_last_modified_header(graph_uri, sparql_url):
    modified = last_modified(graph_uri, sparql_url)
    response.set_header('Last-Modified', http_date(modified))

def entities_used_by(graph_uri, sparql_url):
    """Find all Entities used in the construction of graph_uri.

    Prerequisite: graph_uri prov:wasGeneratedBy some prov:Activity.

    This is a lot like the transforms_used_by function in the ingest
    module, but it includes all Entities, like for example
    sageprov:DataHandlingCodes.

    """
    query = SPARQLWrapper(sparql_url)
    # As in ingests.transforms_used_by, q.v., we need to look three
    # ingests deep for transforms used.  SPARQL 1.1 supports querying
    # for a chain of properties, but 4store only supports SPARQL 1.0,
    # so we have to explicitly lay things out.
    query.setQuery(
        '''
        PREFIX prov: {prov_uri}
        SELECT DISTINCT ?e WHERE {{
            {uri} prov:wasGeneratedBy ?act .
            ?act a prov:Activity .
            {{
              ?act prov:used ?e .
              ?e a prov:Entity .
            }} UNION {{
              ?other_act prov:wasInformedBy ?act .
              ?other_act prov:used ?e .
              ?e a prov:Entity .
            }} UNION {{
              ?other1_act prov:wasInformedBy ?act .
              ?other2_act prov:wasInformedBy ?other1_act .
              ?other2_act prov:used ?e .
              ?e a prov:Entity .
            }}
        }}
        '''.format(
            prov_uri=URIRef(PROV).n3(),
            uri=graph_uri.n3()))
    query.setReturnFormat(JSON)
    results = query.query().convert()
    for r in results['results']['bindings']:
        yield URIRef(r['e']['value'])

def generate_etag(graph_uri, sparql_url):
    es = sorted(list(entities_used_by(graph_uri, sparql_url)))
    # this is strong
    return safen_hash(raw_hash('\n'.join(es)))

def set_etag_header(graph_uri, sparql_url):
    response.set_header('ETag', generate_etag(graph_uri, sparql_url))

document_html_fastgraph_bla = defaultdict(lambda: ((),()))
document_html_fastgraph_bla.update({
    CCILOD.CCIList: (
        (RDFS.label, RDFS.comment), ()),
    XCCDFLOD.Checklist: (
        (RDFS.label, RDFS.comment, 
         XCCDFLOD.hasGroup, XCCDFLOD.hasRule,
         XCCDFLOD.severity), ()),
    SP80053LOD.NistSP80053: (
        (RDFS.label, RDFS.comment,
         SP80053LOD.hasStatement, SP80053LOD.hasEnhancement,
         SP80053LOD.description),
        (SP80053LOD.hasStatement, SP80053LOD.hasEnhancement,
         SP80053LOD.description)),
})

def document_html(document_uri, document_classes):
    template_namespace = _prepare_template_namespace(document_uri, document_classes)
    template_for_class = {
        CCILOD.CCIList: 'ccilod/ccilist/full.html',
        XCCDFLOD.Checklist: 'xccdflod/checklist/full.html',
        SP80053LOD.NistSP80053: 'sp80053lod/nistsp80053/full.html',
    }
    for c in document_classes:
        if c in template_for_class:
            template_name = template_for_class[c]
            break
    else:
        raise HTTPError(500, 'Could not find template for document {0} with '
                        'classes {1}'.format(
                            document_uri,
                            ', '.join(x.n3() for x in document_classes)))
    rendered = template(template_name, **template_namespace)
#    log.debug('queries done:\n%s', pprint.pformat(dict(fastgraph.queryProfile)))
    return rendered


@app.route('/<hash>')
@app.route('/<hash>/')
@with_mimerender(default='html',
             html=document_html,
             turtle=EntireGraphTurtle(),
             rdf=EntireGraphRDFXML())
def document(hash):
    log = logging.getLogger('document')
    document_uri = Xid[hash]
    document_classes = get_classes_or_404(document_uri)
    set_last_modified_header(document_uri, sparql_url)
    set_etag_header(document_uri, sparql_url)
    return {'document_uri': document_uri, 'document_classes': document_classes}



def item_html(item_uri, item_classes, document_uri, document_classes):
    log = logging.getLogger(__name__ + '.item_html')
    template_for_class = {
        XCCDFLOD.Group: 'xccdflod/group/full.html',
        XCCDFLOD.Rule: 'xccdflod/rule/full.html',
        XCCDFLOD.Profile: 'xccdflod/profile/full.html',
        CCILOD.CCIItem: 'ccilod/item/full.html',
        SP80053LOD.Control: 'sp80053lod/control/full.html',
        SP80053LOD.Enhancement: 'sp80053lod/enhancement/full.html',
        SP80053LOD.Statement: 'sp80053lod/statement/full.html',
    }
    for c in item_classes:
        if c in template_for_class:
            template_name = template_for_class[c]
            break
    else:
        raise HTTPError(500, 'Could not find template for item {0} with '
                        'classes {1}'.format(
                            item_uri,
                            ', '.join(x.n3() for x in item_classes)))
    template_namespace = _prepare_template_namespace(document_uri,
                                                     document_classes,
                                                     item_uri=item_uri)
    rendered = template(template_name, **template_namespace)
    return rendered

@app.route('/<doc_hash>/<item_id>')
@with_mimerender(default='html',
                 html=item_html,
                 turtle=ItemTurtle(),
                 rdf=ItemRDFXML())
def item(doc_hash, item_id):
    log = logging.getLogger(__name__ + '.item')
    document_uri = Xid[doc_hash]
    document_classes = get_classes_or_404(document_uri)
    log.debug('document_classes are %r', document_classes)
    document_ns = Namespace(document_uri + '/')
    set_last_modified_header(document_uri, sparql_url)
    set_etag_header(document_uri, sparql_url)
    item_uri = document_ns[item_id]
    item_classes = get_classes_or_404(item_uri)
    return {'item_uri': item_uri, 'item_classes': item_classes,
            'document_uri': document_uri, 'document_classes': document_classes}


