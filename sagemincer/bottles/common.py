# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bottle import response, HTTPError, mako_template, static_file
from sagemincer.fastgraph import get_classes
import functools
import pkg_resources

class GraphResponder(object):
    """Return a CONSTRUCT or DESCRIBE response from 4store directly.

    """
    # override these
    get_query_response = None
    mime_type = None

    def __call__(self, **kwargs):
        if kwargs.has_key('document_uri'):
            kwargs['graph_uri'] = kwargs['document_uri']
        qr = self.get_query_response(**kwargs)
        # Disambiguation: qr.response is an HTTPResponse or so, a
        # file-like object with headers accessible via
        # .info(). response is the Bottle response object.
        response['Content-Type'] = '{}; charset=UTF-8'.format(self.mime_type)
        for k, v in qr.response.info().items():
            # don't leak 4store version
            if k.lower() != 'server':
                response.add_header(k, v)
        return qr.response

class RDFXMLGraphResponder(GraphResponder):
    mime_type = 'application/rdf+xml'

class TurtleGraphResponder(GraphResponder):
    mime_type = 'text/turtle'


# credit to rdflib-web. but unlike rdflib-web, we don't use this value to
# uniquely identify a resource across our entire area of interest.
def _fragment(uri):
    return uri[max(uri.rfind("/"), uri.rfind("#"))+1:]

def get_classes_or_404(thing_uri):
    try:
        return get_classes(thing_uri)
    except KeyError:
        raise HTTPError(404, 'Could not find the classes of {}'.format(thing_uri))

template = functools.partial(mako_template, 
                             template_lookup=[pkg_resources.resource_filename(
                                 'sagemincer.bottles', 'templates')])

def static(path):
    return static_file(path, root=pkg_resources.resource_filename(
        'sagemincer', 'bottles/static'))

def add_static(app):
    app.route('/static/<path:re:.+>', name='static')(static)
