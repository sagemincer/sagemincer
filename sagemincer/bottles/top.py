# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import bottle
import logging
import urllib2
import os
import pkg_resources
from sagemincer.environment import size_url
import mimerender
from sagemincer.bottles.common import template, add_static

try:
    mimerender.register_mime('turtle', ('text/turtle', 'application/x-turtle'))
except MimeRenderException:
    # already registered
    pass

app = bottle.default_app()

add_static(app)

@app.route('/')
def index():
    return template('top_index.html', get_url=app.get_url)

@app.route('/size')
def size():
    logging.getLogger('wsgi').info('index!')
    return urllib2.urlopen(size_url).read()

import sagemincer.bottles.about
app.mount('/about', sagemincer.bottles.about.app)

import sagemincer.bottles.list
app.mount('/list', sagemincer.bottles.list.app)

import sagemincer.bottles.id
app.mount('/id', sagemincer.bottles.id.app)

import sagemincer.bottles.ns
app.mount('/ns', sagemincer.bottles.ns.app)

