# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bottle import Bottle, request, response, HTTPResponse, HTTPError, static_file
from bottle import mako_template
from sagemincer.environment import sparql_url
from sagemincer.bottles.common import RDFXMLGraphResponder, TurtleGraphResponder
from sagemincer.bottles.common import _fragment
from rdflib import RDFS, URIRef
import sagemincer
from sagemincer import Xid, XCCDFLOD, CCILOD, SP80053LOD, STIGLOD, PROV, SAGEPROV
from SPARQLWrapper import SPARQLWrapper, JSON, XML
import mimerender
import logging
import os.path
import time
import pkg_resources
from sagemincer.fastgraph import FastGraph
import isodate
import functools
from urlparse import urlparse

with_mimerender = mimerender.BottleMimeRender()

template = functools.partial(mako_template, 
                             template_lookup=[pkg_resources.resource_filename(
                                 'sagemincer.bottles', 'templates')])

app = Bottle()

# vvv request timing, from http://bottlepy.org/docs/0.12/plugindev.html
def stopwatch(callback):
    def wrapper(*args, **kwargs):
        log = logging.getLogger(__name__ + '.' + callback.func_name)
        start = time.time()
        body = callback(*args, **kwargs)
        end = time.time()
        log.debug('request served in %g sec', end - start)
        return body
    return wrapper
app.install(stopwatch)
# ^^^ request timing


@app.route('/static/:path#.+#', name='static')
def static(path):
    return static_file(path, root=pkg_resources.resource_filename(
        'sagemincer', 'bottles/static'))



list_namespaces = {
    CCILOD.CCIList: CCILOD,
    SP80053LOD.NistSP80053: SP80053LOD,
    XCCDFLOD.Checklist: XCCDFLOD,
}


def _facts_for_list(document_class):
    log = logging.getLogger(__name__ + '._list_graph')
    s = SPARQLWrapper(sparql_url)
    queryText = '''
        PREFIX doctype_ns: <{document_type_namespace}>
        PREFIX xccdflod: <{xccdflod}>
        PREFIX stiglod: <{stiglod}>
        PREFIX rdfs: <{rdfs}>
        PREFIX prov: <{prov}>
        PREFIX sageprov: <{sageprov}>
        SELECT DISTINCT ?d ?title ?firstseen ?lateststatus ?latestdate ?gotfrom ?localfilename ?version ?releasenumber WHERE {{
            ?d a <{document_class}> .
            ?d doctype_ns:hasToplevel ?top .
            OPTIONAL {{ ?top xccdflod:latestDate ?latestdate }} .
            OPTIONAL {{ ?top xccdflod:latestStatus ?lateststatus }} .
            OPTIONAL {{ ?top xccdflod:version ?version }} .
            OPTIONAL {{ ?top stiglod:releaseNumber ?releasenumber }} .
            ?d prov:wasGeneratedBy ?act .
            ?act prov:endedAtTime ?firstseen .
            ?d prov:wasDerivedFrom ?of .
            ?of a sageprov:ObtainedFile .
            OPTIONAL {{
              ?of prov:wasGeneratedBy ?oact .
              ?oact a sageprov:Download .
              ?oact prov:used ?re .
              ?re a prov:Entity .
              ?re prov:atLocation ?gotfrom }} .
            OPTIONAL {{
              ?of sageprov:hadFilename ?localfilename }} .
            OPTIONAL {{ ?top rdfs:label ?title }}
        }}
        ORDER BY ?title ?latestdate
        '''.format(
            document_type_namespace=list_namespaces[document_class],
            xccdflod=XCCDFLOD, stiglod=STIGLOD, rdfs=RDFS, prov=PROV,
            sageprov=SAGEPROV, document_class=document_class)
    log.debug(queryText)
    s.setQuery(queryText)
    s.setReturnFormat(JSON)
    s.addCustomParameter('soft-limit', '-1')
    result = s.query().convert()
    log.debug('%d results', len(result['results']['bindings']))
    for row in result['results']['bindings']:
        toplabel = row['title'].get('value', 'untitled')
        for rep in (
                ('Security Technical Implementation Guide (STIG)', 'STIG'),
                ('Security Technical Implementation Guide', 'STIG'),
                ('(STIG)', 'STIG'),
                ('Secure Technical Implementation Guide', 'STIG'),
                ('SECURITY TECHNICAL IMPLEMENTATION GUIDE', 'STIG'),
                ('Security Implementation Guide', 'STIG'),
                ('(SRG)', 'SRG'),
                ('Security Requirements Guide', 'SRG'),
                ('Interim Security Configuration Guide', 'ISCG'),
        ):
            toplabel = toplabel.replace(rep[0], rep[1])
        toplabel = toplabel.strip()
        if row['gotfrom'].has_key('value'):
            original_filename = urlparse(row['gotfrom']['value'])[2].split('/')[-1]
        elif row['localfilename'].has_key('value'):
            # don't leak directory name: web users don't need it
            original_filename = os.path.basename(row['localfilename']['value'])
        else:
            # this is a human interface; if we can't show a thing, don't sweat it
            original_filename = '(unknown)'
        yield {
            'd': row['d']['value'],
            'id': _fragment(row['d']['value']),
            'filename': original_filename,
            'firstseen': isodate.parse_date(row['firstseen']['value']),
            'lateststatus': row['lateststatus'].get('value', '(unknown)'),
            'latestdate': (isodate.parse_date(row['latestdate']['value'])
                           if row['latestdate'].has_key('value')
                           else '(unknown)'),
            'version': row['version'].get('value', '?'),
            'releasenumber': row['releasenumber'].get('value', '?'),
            'toplabel': toplabel,
        }


def _graph_query_for_list(document_class):
    log = logging.getLogger(__name__ + '._graph_query_for_list')
    s = SPARQLWrapper(sparql_url)
    queryText = '''
        PREFIX doctype_ns: <{document_type_namespace}>
        PREFIX xccdflod: <{xccdflod}>
        PREFIX rdfs: <{rdfs}>
        CONSTRUCT {{
            ?d a <{document_class}> .
            ?d doctype_ns:hasToplevel ?top .
            ?d xccdflod:firstSeen ?firstseen .
            ?d xccdflod:filenameIs ?filename .
            ?top rdfs:label ?title
        }} WHERE {{
            ?d a <{document_class}> .
            ?d doctype_ns:hasToplevel ?top .
            ?d xccdflod:firstSeen ?firstseen .
            ?d xccdflod:filenameIs ?filename
            OPTIONAL {{ ?top rdfs:label ?title }}
        }}
        '''.format(
            document_type_namespace=list_namespaces[document_class],
            xccdflod=XCCDFLOD, rdfs=RDFS,
            document_class=document_class)
    log.debug(queryText)
    s.setQuery(queryText)
    s.addCustomParameter('soft-limit', '-1')
    return s

def rdfxml_query_for_list(*args, **kwargs):
    s = _graph_query_for_list(kwargs['document_class'])
    s.setReturnFormat(XML)
    return s.query()

def turtle_query_for_list(*args, **kwargs):
    s = _graph_query_for_list(kwargs['document_class'])
    s.setReturnFormat(JSON)
    return s.query()

class ListRDFXML(RDFXMLGraphResponder):
    get_query_response = rdfxml_query_for_list

class ListTurtle(TurtleGraphResponder):
    get_query_response = turtle_query_for_list

def list_html(document_class):
    log = logging.getLogger(__name__ + '.list_html')
    name = {
        XCCDFLOD.Checklist: 'xccdflod/checklist/list.html',
        CCILOD.CCIList: 'ccilod/ccilist/list.html',
        SP80053LOD.NistSP80053: 'sp80053lod/nistsp80053/list.html',
    }[document_class]
    rendered = template(name,
        the_documents=_facts_for_list(document_class),
        get_url=app.get_url,
    )
    return rendered

@app.route('/<stem>')
@app.route('/<stem>/')
@with_mimerender(default='html',
                 html=list_html,
                 turtle=ListTurtle(),
                 rdf=ListRDFXML())
def list(stem):
    log = logging.getLogger(__name__ + '.list')
    document_class = {
        'cci': CCILOD.CCIList,
        '80053': SP80053LOD.NistSP80053,
        'cl': XCCDFLOD.Checklist,
    }[stem]
    return {
        'document_class': document_class,
    }


