import pkg_resources
from FuXi.Rete.RuleStore import SetupRuleStore
from FuXi.Rete.Util import generateTokenSet
from FuXi.Horn.HornRules import HornFromN3
from rdflib import Graph

rdfs_rules_n3_stream = pkg_resources.resource_stream('sagemincer',
        'ontologies/rdfs-rules.n3')

# https://code.google.com/p/fuxi/wiki/Tutorial
def forward_chain_rdfs(ruley_facts, facty_facts):
    rule_store, rule_graph, network = SetupRuleStore(makeNetwork=True)
    network.inferredFacts = Graph()
    for rule in HornFromN3(rdfs_rules_n3_stream):
        network.buildNetworkFromClause(rule)
    network.feedFactsToAdd(generateTokenSet(ruley_facts))
    network.feedFactsToAdd(generateTokenSet(facty_facts))
    return network.inferredFacts
