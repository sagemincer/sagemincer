# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import re
import requests
import os.path
from sagemincer import PROV, SAGEPROV, Xdownload, Xinduct
from sagemincer import hash_algorithm, safen_hash, hash_property
from sagemincer import raw_hash
from sagemincer.to_4store import post_to_graph_turtle
from rdflib import Graph, RDF, Namespace
from rdflib.term import URIRef, Literal
from sagemincer.environment import download_cache_dir
from sagemincer.fs_sparql import SPARQLWrapper, JSON
import uuid
import datetime
from errno import EEXIST

# originally from lengthy_download
def _name_unsafe_string(s):
    """Compose a safe, meaningful filename corresponding to s.

    Safe means that the name has a bounded length, but will still
    certainly differ for different values of s; and it doesn't
    contain any characters that are meaningful to a filesystem or
    a shell, like a star, a slash, a tilde, a pound sign, or a
    colon; and it doesn't contain any non-ASCII characters.
    Meaningful means it has some portion of s in it, so a person
    can tell something about s from the name.

    """
    clamped_name = (s[:21] + '...' + s[-21:]
                    if len(s) > 42
                    else s)
    safe_name = re.sub('[^A-Za-z0-9.]', '_', clamped_name)
    safe_hash = safen_hash(raw_hash(
        s.encode('UTF-8'))).decode('ascii')
    return safe_name + '-' + safe_hash


class UnexpectedHTTPResponse(Exception):
    pass

# this one is random
NAMESPACE_UUID = uuid.UUID('b909ed8d-08a0-4448-a126-9be641e1c4c9')

def fetch(log, url, sparql_url, data_post_url):
    log = log.getChild('fetch')
    w = SPARQLWrapper(sparql_url)
    w.setQuery(
        '''PREFIX sageprov: {sageprov_ns}
           PREFIX prov: {prov_ns}
          SELECT ?df ?etag WHERE {{
          ?df a sageprov:DownloadedFile .
          ?d a sageprov:Download .
          ?d prov:startedAtTime ?t .
          ?df prov:wasGeneratedBy ?d .
          ?d sageprov:hadETag ?etag .
          ?d prov:used ?e .
          ?e prov:atLocation {url} .
        }} ORDER BY DESC(?t)'''.format(
            sageprov_ns=URIRef(SAGEPROV).n3(),
            prov_ns=URIRef(PROV).n3(),
            url=URIRef(url).n3()))
    w.setReturnFormat(JSON)
    # if we have failed to fetch this fully before for any reason, we
    # won't have gotten all the way to the bottom of this function,
    # and so the etag will not be written in the triplestore.
    known_etags_results = w.query().convert()
    known_etags = set(res['etag']['value'] for res in known_etags_results['results']['bindings'])
    completed_downloads = [res['df']['value'] for res in known_etags_results['results']['bindings']]
    log.debug('we have seen these ETags before: %r', known_etags)
    # Empirically, DISA's web server does not know how to handle multiple
    # If-None-Match values when we do a conditional GET. So we just HEAD first.
    #
    # In 2018, STIGs are being served from somewhere else, with a 301 Moved
    # Permanently redirect. But we are fetching the URLs that DISA linked to,
    # so the 301s are sort of an implementation detail, which if remembered may
    # become stale. Meanwhile, requests follows redirects by default, except
    # for HEAD requests, which this one is. I'm going to ignore the fact that
    # the paragraph just above is probably not true anymore, and just let HEAD
    # follow the 301 explicitly.
    head_response = requests.head(url, allow_redirects=True)
    head_response.raise_for_status()
    if head_response.headers['etag'] in known_etags:
        # we don't need to fetch or write anything.
        return
    start_time = datetime.datetime.utcnow()
    get_response = requests.get(url)
    get_response.raise_for_status()
    hash = hash_algorithm()
    fetched_etag = get_response.headers['ETag']
    fetched_content_type = get_response.headers['Content-Type']
    dirname_for_this_url = os.path.join(download_cache_dir,
                                        _name_unsafe_string(url))
    try:
        os.makedirs(dirname_for_this_url)
    except OSError, e:
        if e.errno == EEXIST:
            pass
        else:
            raise
    filename_for_this_fetch = (start_time.strftime('%Y-%m-%dT%H:%M:%SZ') + '-' + 
                               safen_hash(raw_hash(fetched_etag)))
    fullpath = os.path.join(dirname_for_this_url,
                            filename_for_this_fetch)
    if get_response.status_code == requests.codes.ok:
        with open(fullpath, 'wb') as f:
            for chunk in get_response.iter_content(65536):
                f.write(chunk)
                hash.update(chunk)
    else:
        # unexpected, but not 4xx or 5xx. Won't be 304 Not Modified, because
        # we already checked the ETag in the HEAD.
        raise UnexpectedHTTPResponse('when fetching', url,
                                         get_response.status_code)
    end_time = datetime.datetime.utcnow()
    unique_bits = "---\nURL: {0}\nETag: {1}\nContent-Type: {2}\n".format(
        url, fetched_etag, fetched_content_type)
    log.debug('unique bits are %r', unique_bits)
    safe_hash = safen_hash(hash.digest())
    uuid5 = uuid.uuid5(NAMESPACE_UUID, unique_bits)
    ns = Namespace(Xdownload[str(uuid5)] + '/')
    log.debug('I dub this download %s', ns)
    g = Graph()
    g.add((ns.file,          RDF.type,               SAGEPROV.DownloadedFile))
    g.add((ns.file,          RDF.type,               SAGEPROV.ObtainedFile))
    g.add((ns.file,          RDF.type,               PROV.Entity))
    g.add((ns.file,          SAGEPROV.hadFilename,   Literal(fullpath)))
    g.add((ns.file,          hash_property,          Literal(safe_hash)))
    # thanks to the ORDERing, the latest completed download is first
    # in the list; this is the one that this download revises.
    if len(completed_downloads) > 0:
        g.add((ns.file,          PROV.wasRevisionOf,     URIRef(completed_downloads[0])))
    g.add((ns.act,           RDF.type,               SAGEPROV.Download))
    g.add((ns.act,           RDF.type,               SAGEPROV.ObtainContent))
    g.add((ns.act,           RDF.type,               PROV.Activity))
    g.add((ns.act,           SAGEPROV.hadETag,       Literal(fetched_etag)))
    g.add((ns.act,           SAGEPROV.hadContentType, Literal(fetched_content_type)))
    g.add((ns.act,           PROV.startedAtTime,     Literal(start_time)))
    g.add((ns.act,           PROV.endedAtTime,       Literal(end_time)))
    g.add((ns.file,          PROV.wasGeneratedBy,    ns.act))
    g.add((ns.remoteEntity,  RDF.type,               PROV.Entity))
    g.add((ns.remoteEntity,  PROV.atLocation,        URIRef(url)))
    g.add((ns.act,           PROV.used,              ns.remoteEntity))
    post_to_graph_turtle(data_post_url, ns.graph, g)
    

def chunks_of(fileobject, size=65536):
    chunk = fileobject.read(size)
    while len(chunk) == size:
        yield chunk
        chunk = fileobject.read(size)
    yield chunk


def induct(log, filename, sparql_url, data_post_url):
    filename = os.path.abspath(filename)
    log = log.getChild('induct_from_cache')
    w = SPARQLWrapper(sparql_url)
    w.setQuery(
        '''PREFIX sageprov: {sageprov_ns}
           PREFIX prov: {prov_ns}
          SELECT ?df ?et WHERE {{
          ?df a sageprov:ObtainedFile .
          ?d a sageprov:ObtainContent .
          ?d prov:startedAtTime ?t .
          OPTIONAL {{ 
            ?d prov:endedAtTime ?et
          }} .
          ?df prov:wasGeneratedBy ?d .
          ?df sageprov:hadFilename ?fn .
        }} ORDER BY DESC(?t)'''.format(
            sageprov_ns=URIRef(SAGEPROV).n3(),
            prov_ns=URIRef(PROV).n3(),
            fn=filename))
    w.setReturnFormat(JSON)
    downloaded_results = w.query().convert()
    # these should be called completed_inductions; the name remains
    # the same so that you can easily diff this with fetch
    completed_downloads = [res['df']['value'] for res in
                           downloaded_results['results']['bindings']
                           if len(res['et']) > 0]
    log.debug('we have seen these completed inductions before: %r',
              completed_downloads)
    # if we got more than 0, it's likely we've inducted this file
    # already. but check the checksum too before bailing out
    start_time = datetime.datetime.utcnow()
    hash = hash_algorithm()
    with file(filename) as f:
        for chunk in chunks_of(f):
            hash.update(chunk)
    safe_hash = safen_hash(hash.digest())
    end_time = datetime.datetime.utcnow()
    unique_bits = "---\nFilename: {0}\n{1}: {2}\n".format(
        filename, hash_property, safe_hash)
    log.debug('unique bits are %r', unique_bits)
    uuid5 = uuid.uuid5(NAMESPACE_UUID, unique_bits)
    ns = Namespace(Xinduct[str(uuid5)] + '/')
    if ns.file in completed_downloads:
        # we don't need to fetch or write anything.
        log.info('file %r with %s %s has already been inducted; '
                 'doing nothing', filename, hash_property, safe_hash)
        return
    # otherwise, maybe we have seen this file before with a different
    # checksum, or maybe we have not seen it - whatever it is, we need
    # to induct it. FIXME: do we need to delete things stemming from
    # files previously ingested files having this filename but a
    # different checksum?
    log.debug('I dub this induction %s', ns)
    g = Graph()
    g.add((ns.file,          RDF.type,               SAGEPROV.InductedFile))
    g.add((ns.file,          RDF.type,               SAGEPROV.ObtainedFile))
    g.add((ns.file,          RDF.type,               PROV.Entity))
    g.add((ns.file,          SAGEPROV.hadFilename,   Literal(filename)))
    g.add((ns.file,          hash_property,          Literal(safe_hash)))
    # we aren't going to have revisions of manually added files,
    # because we aren't saving copies of them. new manually added
    # files must have different names.
    g.add((ns.act,           RDF.type,               SAGEPROV.InductManuallyAdded))
    g.add((ns.act,           RDF.type,               SAGEPROV.ObtainContent))
    g.add((ns.act,           RDF.type,               PROV.Activity))
    g.add((ns.act,           PROV.startedAtTime,     Literal(start_time)))
    g.add((ns.act,           PROV.endedAtTime,       Literal(end_time)))
    g.add((ns.file,          PROV.wasGeneratedBy,    ns.act))
    post_to_graph_turtle(data_post_url, ns.graph, g)
    
