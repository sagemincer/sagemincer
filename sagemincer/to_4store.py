# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import urllib, urllib2
from sagemincer.url_join import url_path_join
from sagemincer import user_agent_string
from rdflib import Graph

def put_graph_rdfxml(data_put_url, graph_uri, rdfxml):
    log = logging.getLogger('put_graph_rdfxml')
    to_url = url_path_join(data_put_url, urllib.quote(graph_uri))
#    log.debug('PUTting graph to %s', to_url)
#    log.debug('data:')
#    log.debug(rdfxml)
#    return
    # http://stackoverflow.com/questions/9867092
    opener = urllib2.build_opener(urllib2.HTTPHandler)
    request = urllib2.Request(to_url, rdfxml,
                              headers={'User-Agent': user_agent_string})
    request.get_method = lambda: 'PUT'
    url = opener.open(request)
    url.read()

def delete_graph(data_put_url, graph_uri):
    log = logging.getLogger('delete_graph')
    to_url = url_path_join(data_put_url, urllib.quote(graph_uri))
    log.debug('Deleting graph from %s', to_url)
    # http://stackoverflow.com/questions/9867092
    opener = urllib2.build_opener(urllib2.HTTPHandler)
    request = urllib2.Request(to_url, '',
                              headers={'User-Agent': user_agent_string})
    request.get_method = lambda: 'DELETE'
    url = opener.open(request)
    url.read()

def post_to_graph_rdfxml(data_put_url, graph_uri, rdfxml):
    log = logging.getLogger('post_to_graph_rdfxml')
    to_url = url_path_join(data_put_url, urllib.quote(graph_uri))
    log.debug('POSTing graph to %s', to_url)
#    log.debug('data:')
#    log.debug(rdfxml)
#    return
    # http://stackoverflow.com/questions/9867092
    params = urllib.urlencode({'graph': graph_uri,
                               'data': rdfxml,
                               'mime-type': 'application/rdf+xml'})
    opener = urllib2.build_opener(urllib2.HTTPHandler)
    request = urllib2.Request(data_put_url, params,
                              headers={'User-Agent': user_agent_string})
    request.get_method = lambda: 'POST'
    url = opener.open(request)
    url.read()

def post_to_graph_turtle(data_put_url, graph_uri, g):
    log = logging.getLogger('post_to_graph_turtle')
    to_url = url_path_join(data_put_url, urllib.quote(graph_uri))
    turtle = g.serialize(format='turtle')
    log.debug('POSTing graph to %s', to_url)
#    log.debug('data:')
#    log.debug(turtle)
#    return
    # http://stackoverflow.com/questions/9867092
    params = urllib.urlencode({'graph': graph_uri,
                               'data': turtle,
                               'mime-type': 'text/turtle'})
    opener = urllib2.build_opener(urllib2.HTTPHandler)
    request = urllib2.Request(data_put_url, params,
                              headers={'User-Agent': user_agent_string})
    request.get_method = lambda: 'POST'
    url = opener.open(request)
    url.read()

def store_in_4store_by_PUT(rdfxmls, data_put_url, i_have_seen_it):
    log = logging.getLogger('top')
    for filename, contents_hash, checklist_uri, rdfxml in rdfxmls:
        put_graph_rdfxml(data_put_url, checklist_uri, rdfxml)
        i_have_seen_it(filename, contents_hash)
