# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from sagemincer.environment import sparql_url
from SPARQLWrapper import SPARQLWrapper, JSON, XML
from rdflib import URIRef, Namespace
import logging
import time
from collections import defaultdict
import pprint

class RaiseExceptionSentinel(object):
    pass

class FastGraph(object):
    """Cache results gotten using SPARQL in a quick, flexible way.

    When we get RDF/XML or Turtle out of 4store, then hand it to
    rdflib to parse into a Graph, 4store responds within hundredths of
    a second, then rdflib takes seconds to tens of seconds to parse
    the results. That's not fast enough to render a web page
    responsively. So here we fetch those results as JSON, and put them
    into dictionaries by predicate. This seems much faster for our
    case.
    
    It's instructive to note the specificities of our case: we are
    dealing here in a single graph out of many in the
    triplestore. This graph has tens of thousands of triples; we have
    only dozens of different predicates, and for each predicate
    hundreds of triples. The objects of more than half the triples are
    strings, which we want to look up and place into the text of the
    web page. We're not going to use all the facts in the graph to
    make the web page.

    """

    # If no list of predicates to cache is given, and there are more
    # than this many triples in this graph having a single predicate,
    # we cache all the values of that predicate in this graph.
    COMMON_PREDICATES_HAVE = 100

    def __init__(self, graph_uri, cache_predicates=(),
                 only_consult_cache_predicates=()):
        """Construct a FastGraph, including fetching its cache.

        If cache_predicates is given, it is a tuple of predicates to
        cache. Otherwise, all predicates in the graph which occur in
        more than a certain number of triples are cached.

        If only_consult_cache_predicates is given, it is a tuple of
        predicates about which the answers in the cache are to be
        taken as the only answers. These correspond to triples which
        should only ever be found inside the named graph. For example,
        in the graph derived from a NIST SP 800-53 document, a
        hasStatement predicate we are interested in is only ever
        inside the graph, and only points inside the graph. So if we
        cache data for the hasStatement predicate, and then ask a
        question about that predicate, and don't find the answer in
        the cache, we shouldn't run a query to find the answer, but
        should just fail to find an answer. A counterexample is if we
        want to find security requirements that point to controls in a
        NIST SP 800-53 document. Those will be written in other named
        graphs than the one for the document, and we want to query to
        find all those.

        """
        self.queryProfile = defaultdict(lambda: 0)
        self.graph_uri = graph_uri
        self.log = logging.getLogger(repr(self))
        self._by_subject = {}
        self._by_object = {}
        t1 = time.time()
        self._construct_cache(cache_predicates)
        t2 = time.time()
        self.log.debug('cache construction took %g sec', t2-t1)
        self.only_consult_cache_predicates = set(only_consult_cache_predicates)
        
    def __repr__(self):
        return '<{} {}>'.format(self.__class__.__name__, self.graph_uri)

    def _select(self, explanation, query_text):
        self.queryProfile[explanation] += 1
#        self.log.debug('query: %s', query_text)
        w = SPARQLWrapper(sparql_url)
#        w.addCustomParameter('soft-limit', '-1')
        w.setReturnFormat(JSON)
        w.setQuery(query_text)
        return w.query().convert()

    def _cacheable_predicates(self):
        qr = self._select('_cacheable_predicates',
            '''SELECT ?p (count(?p) as ?c)
            WHERE {{ GRAPH <{guri}> {{ ?s ?p ?o }} }}
            GROUP BY ?p ORDER BY DESC(?c)'''.format(
                guri=self.graph_uri))
        for bindings in qr['results']['bindings']:
            predicate = URIRef(bindings['p']['value'])
            count = int(bindings['c']['value'])
            # HAVING didn't appear to work under 4store 1.1.5+vsr, but
            # ORDER BY did
            if count < self.COMMON_PREDICATES_HAVE:
                break
            else:
                yield predicate

    def _construct_cache(self, cache_predicates=()):
        if cache_predicates == ():
            self.log.debug('Finding cacheable predicates')
            cache_predicates = self._cacheable_predicates()
        for p in cache_predicates:
#            self.log.debug('Caching predicate %s', p)
            qr = self._select('_construct_cache {}'.format(p),
                '''SELECT DISTINCT ?s ?o WHERE {{
                GRAPH <{guri}> {{ ?s {p} ?o }} }}'''.format(
                    guri=self.graph_uri, p=p.n3()))
            bindings = qr['results']['bindings']
            self._by_subject[p] = {}
            self._by_object[p] = {}
            p_by_subject = self._by_subject[p]
            p_by_object = self._by_object[p]
            for row in bindings:
                subject = URIRef(row['s']['value'])
                p_by_subject.setdefault(subject, [])
                object = row['o']['value']
                if row['o']['type'] == 'uri':
                    object = URIRef(object)
                    p_by_object.setdefault(object, [])
                    p_by_object[object].append(subject)
                    p_by_subject[subject].append(object)
                else:
                    # we're not looking up things that have :flarble
                    # 42, so don't add to p_by_object.
                    #
                    # FIXME we could be more sophisticated about the
                    # type of the data
                    p_by_subject[subject].append(object)

    def getvalue(self, s, p, default=RaiseExceptionSentinel):
        for o in self._by_subject.get(p,{}).get(s,[]):
            return o
        if p in self.only_consult_cache_predicates:
            # it's not in the cache. we are given to assume it does
            # not exist
            pass
        else:
            # not in cache. maybe outside this graph
            qr = self._select('SINGLE getvalue {}'.format(p),
                '''SELECT ?o WHERE {{ <{s}> <{p}> ?o }}'''.format(
                    s=s, p=p))
            bindings = qr['results']['bindings']
            for row in bindings:
                return valuify(row['o'])
        # not in cache and not found via query.
        if default is RaiseExceptionSentinel:
            raise KeyError(s,p)
        else:
            return default

    def objects(self, s, p):
        any = False
        for o in self._by_subject.get(p,{}).get(s,[]):
            any = True
            yield o
        if not any:
            if p in self.only_consult_cache_predicates:
                # none were in the cache; assume none exist
                pass
            else:
                # not in cache. maybe outside this graph
                qr = self._select('SINGLE objects {}'.format(p),
                    '''SELECT ?o WHERE {{ <{s}> <{p}> ?o }}'''.format(
                        s=s, p=p))
                bindings = qr['results']['bindings']
                for row in bindings:
                    yield valuify(row['o'])

    def subjects(self, p, o):
        any = False
        for s in self._by_object.get(p,{}).get(o,[]):
            any = True
            yield s
        if not any:
            if p in self.only_consult_cache_predicates:
                # none were in the cache; assume none exist
                pass
            else:
                # not in cache. maybe outside this graph
                qr = self._select('SINGLE subjects {}'.format(p),
                    '''SELECT ?s WHERE {{ ?s <{p}> <{o}> }}'''.format(
                        p=p, o=o))
                bindings = qr['results']['bindings']
                for row in bindings:
                    yield valuify(row['s'])

    def partial_getvalue(self, p, default=None):
        return lambda s: self.getvalue(s, p, default)

    def partial_objects(self, p):
        return lambda s: self.objects(s, p)

    def partial_subjects(self, p):
        return lambda o: self.subjects(p, o)


# WARNING: the effect of this function is inlined in the
# _construct_cache above, without calling, so if you change this you
# still have more to change
def valuify(row):
    if row['type'] == 'uri':
        return URIRef(row['value'])
    else:
        # FIXME: make more sophisticated
        return row['value']



def _query_for_graph(graph_uri, format):
    log = logging.getLogger(__name__+'_get_graph')
    t1 = time.time()
    s = SPARQLWrapper(sparql_url)
    text = '''CONSTRUCT {{ ?s ?p ?o }} WHERE {{
        GRAPH <{0}> {{ ?s ?p ?o }} }}'''.format(graph_uri)
    log.debug(text)
    s.setQuery(text)
    s.addCustomParameter('soft-limit', '-1')
    s.setReturnFormat(format)
    qr = s.query()
    return qr


def graph_as_turtle(*args, **kwargs):
    # When we tell SPARQLWrapper we want JSON, and we are running a
    # CONSTRUCT query against a 4store server, we get Turtle back. If
    # we tell SPARQLWrapper we want TURTLE, it doesn't work.
    return _query_for_graph(kwargs['graph_uri'], JSON)

def graph_as_rdfxml(*args, **kwargs):
    return _query_for_graph(kwargs['graph_uri'], XML)


def _describe_item(item_uri, graph_uri, format):
    log = logging.getLogger(__name__+'_describe_item')
    t1 = time.time()
    s = SPARQLWrapper(sparql_url)
    document_ns = Namespace(graph_uri + '/')
    top = document_ns.top
    # we filter out the top of the document because it has hundreds of
    # triples that don't have to do with the item we are concerned
    # with, compared to dozens at most that are pertinent (474KB of
    # Turtle vs 2KB, in one trial)
    text = '''DESCRIBE ?ss ?os <{0}> WHERE {{
        ?ss ?p <{0}> . <{0}> ?p2 ?os 
        FILTER(?ss != <{1}>) }}'''.format(item_uri, top)
    log.debug(text)
    s.setQuery(text)
    s.addCustomParameter('soft-limit', '-1')
    s.setReturnFormat(format)
    qr = s.query()
    return qr

def item_as_turtle(*args, **kwargs):
    # When we tell SPARQLWrapper we want JSON, and we are running a
    # CONSTRUCT query against a 4store server, we get Turtle back. If
    # we tell SPARQLWrapper we want TURTLE, it doesn't work.
    return _describe_item(kwargs['item_uri'], kwargs['graph_uri'], JSON)

def item_as_rdfxml(*args, **kwargs):
    return _describe_item(kwargs['item_uri'], kwargs['graph_uri'], XML)

def get_classes(thing_uri):
    w = SPARQLWrapper(sparql_url)
    w.setQuery('SELECT ?c WHERE {{ <{uri}> a ?c }}'.format(uri=thing_uri))
    w.setReturnFormat(JSON)
    r = w.query().convert()
    thing_classes = set([])
    for row in r['results']['bindings']:
        if row['c']['type'] == 'uri':
            c_uri = URIRef(row['c']['value'])
            thing_classes.add(c_uri)
    if len(thing_classes) == 0:
        raise KeyError(thing_uri)
    else:
        return thing_classes

