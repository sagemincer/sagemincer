from sagemincer import SP80053LOD
from rdflib import RDFS
from SPARQLWrapper import SPARQLWrapper, JSON
from sagemincer.environment import sparql_url
import re
import logging

def find(nist_sp_800_53_v4_uri, reference):
    """Find a NIST SP 800-53 v4 identifier for a reference.

    The CCI list as of fall 2014 doesn't write references to NIST SP
    800-53 v4 the same way the identifiers in the actual NIST SP
    800-53 v4 XML document are written. This function tries to
    reconcile the two.

    """
    log = logging.getLogger(__name__ + '.find')
    # first, see if the reference exactly matches.
    s = SPARQLWrapper(sparql_url)
    queryText = '''
        PREFIX sp80053lod: <{sp80053lod_uri}>
        PREFIX rdfs: <{rdfs_uri}>

        SELECT ?s WHERE {{ GRAPH <{the_graph}> {{
        {{ {{ ?s a sp80053lod:Control }} UNION
           {{ ?s a sp80053lod:Statement }} UNION
           {{ ?s a sp80053lod:Enhancement }} }} .
        ?s rdfs:label {reference}
        }} }}
        '''.format(
            the_graph=nist_sp_800_53_v4_uri,
            sp80053lod_uri=SP80053LOD,
            rdfs_uri=RDFS,
            reference=repr(reference))
#    log.debug(queryText)
    s.setQuery(queryText)
    s.setReturnFormat(JSON)
    s.addCustomParameter('soft-limit', '-1')
    qr = s.query().convert()
    for bindings in qr['results']['bindings']:
        uri = bindings['s']['value']
        log.debug('Exact: reference %r, found %s', reference, uri)
        return uri
    # still here? no exact match.
    #
    # we are going to construct a regex by doing a regex substitution
    # on reference.
    regex = r'^' + re.sub(r'(?<=[A-Za-z0-9]) (?=[A-Za-z0-9]|$)', r'[ .]?',
                          re.sub(r'\)', r'\\\\)',
                                 re.sub(r'\(', r'\\\\(',
                                        re.sub(r'\) \(', r') ?(',
                                               reference)))) + r'\\.?$'
    s = SPARQLWrapper(sparql_url)
    queryText = '''
        PREFIX sp80053lod: <{sp80053lod_uri}>
        PREFIX rdfs: <{rdfs_uri}>

        SELECT ?s WHERE {{ GRAPH <{the_graph}> {{
        {{ {{ ?s a sp80053lod:Control }} UNION
           {{ ?s a sp80053lod:Statement }} UNION
           {{ ?s a sp80053lod:Enhancement }} }} .
        ?s rdfs:label ?l
        FILTER regex(?l, "{regex}")
        }} }}
        '''.format(
            the_graph=nist_sp_800_53_v4_uri,
            sp80053lod_uri=SP80053LOD,
            rdfs_uri=RDFS,
            regex=regex)
#    log.debug(queryText)
    s.setQuery(queryText)
    s.setReturnFormat(JSON)
    s.addCustomParameter('soft-limit', '-1')
    qr = s.query().convert()
    for bindings in qr['results']['bindings']:
        uri = bindings['s']['value']
        log.debug('Regex: reference %r, found %s', reference, uri)
        return uri
    log.error('FAIL: reference %r, could not find a NIST SP 800-53 v4 item in %s',
              reference, nist_sp_800_53_v4_uri)
    raise KeyError(reference)
