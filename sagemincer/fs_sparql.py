import logging
import SPARQLWrapper as sw
from SPARQLWrapper import JSON, JSONLD, XML, TURTLE, N3, RDF

class SPARQLWrapper(sw.SPARQLWrapper):
    """Do SPARQL queries against a 4store server.

    The 4store SPARQL server looks at the Accept header in the request to
    determine the format in which results are desired; query parameters are not
    necessary and can get in the way.
    
    SPARQLWrapper sets a bunch of query parameters, in an attempt to construct
    a query that will work against any SPARQL server. These confuse 4store.

    This class uses the return format set by the user of SPARQLWrapper only to
    construct an Accept header, not to add query parameters.
    """

    def __init__(self, *args, **kwargs):
        self.log = logging.getLogger('SPARQLWrapper')
        super(SPARQLWrapper, self).__init__(*args, **kwargs)

    def _getRequestParameters(self):
        queryParameters = self.parameters.copy()
        # The superclass sets the format, output and results query parameters
        # here, according to self.returnFormat. This class does not.
        utfQueryParameters = {}
        for k, vs in queryParameters.items():
            encodedValues = []
            for v in vs:
                if isinstance(v, unicode):
                    encodedValues.append(v.encode('utf-8'))
                else:
                    encodedValues.append(v)
            utfQueryParameters[k] = encodedValues
        return utfQueryParameters

    def query(self, *args, **kwargs):
        self.log.debug('query parameters are %r', self._getRequestParameters())
        self.log.debug('query string is \n%s', self.queryString)
        return super(SPARQLWrapper, self).query(*args, **kwargs)
