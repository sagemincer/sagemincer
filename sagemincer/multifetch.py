# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
import errno
import multiprocessing
from subprocess import Popen, PIPE
from Queue import Empty
import itertools
from sagemincer.environment import (
    sparql_url, data_post_url, setup_logging, phantomjs_executable,
    induct_dir
    )
import logging
import os
from sagemincer.internal_metadata import make_internal_metadata
from sagemincer.onefetch import fetch, induct
from sagemincer.ingest import ingest
from logutils.queue import QueueHandler, QueueListener
import pkg_resources


# My cron jobs on the public OpenShift site get 20 minutes to do their
# business. We'll make it 19 so we surely have time to clean up.
MAX_LIFETIME = 19 * 60

FETCH_WAIT_BETWEEN = 1 # seconds, enough for 540 fetches @ 1 sec each

def config_logging_to_queue(log_queue):
    log_h = QueueHandler(log_queue)
    log_f = logging.Formatter('%(asctime)s %(levelname)s %(module)s: %(message)s')
    log_h.setFormatter(log_f)
    root_logger = logging.getLogger()
    # any handlers we may have had from before (perhaps before a fork)
    # should not be used: we are using the queue instead.
    for h in root_logger.handlers:
        root_logger.removeHandler(h)
    root_logger.addHandler(log_h)


def fetch_process(in_queue, log_queue, sparql_url, data_post_url):
    """For each URL given on in_queue, if new, fetch and notate in database.
    
    URLs to be fetched should be small enough to fetch in less than 30
    seconds.

    This process will be I/O-bound.
    """
    config_logging_to_queue(log_queue)
    log = logging.getLogger('fetch_process')
    while True:
        try:
            url = in_queue.get(True)
        except Empty:
            log.info('queue empty, quitting')
            break
        log.info('check/fetch %s', url)
        try:
            fetch(log, url, sparql_url, data_post_url)
        except KeyboardInterrupt:
            raise
        except:
            log.exception('fetch failed! moving on')
        time.sleep(FETCH_WAIT_BETWEEN)
        in_queue.task_done()

URLTYPE_CCI = 1
URLTYPE_80053 = 2
URLTYPE_STIG = 3

INGEST_WAIT_BETWEEN = 5 # seconds
def ingest_process(quit_queue, log_queue, sparql_url, data_post_url):
    """Look for documents downloaded but not ingested; ingest them.

    The callables are called repeatedly, every INGEST_WAIT_BETWEEN
    seconds.

    """
    config_logging_to_queue(log_queue)
    log = logging.getLogger('ingest_process')
    while True:
        try:
            message = quit_queue.get(False, INGEST_WAIT_BETWEEN)
            log.info('received something on quit_queue, quitting')
            break
        except Empty:
            pass
        log.debug('making a pass')
        ingest(sparql_url, data_post_url)
        time.sleep(INGEST_WAIT_BETWEEN)

def get_stig_urls():
    log = logging.getLogger('get_stig_urls')
    log.debug('began')
    js_filename = pkg_resources.resource_filename('sagemincer',
                                                  'phantomjs/az.js')
    cmd = [phantomjs_executable, js_filename]
    log.info('running %r', cmd)
    try:
        p = Popen(cmd, stdin=None, stdout=PIPE, stderr=None, close_fds=True)
    except OSError, e:
        if e.errno == errno.ENOENT:
            log.error('Could not run the phantomjs executable at %r',
                      phantomjs_executable)
            return
        else:
            raise
    reject_if = (
        ('STIG library; we are already fetching all its pieces',
         lambda line: 'stig_library' in line.lower()),
        ('this is a video, not a STIG',
         lambda line: 'demo_video' in line.lower()),
        ('For Official Use Only',
         lambda line: 'fouo' in line.lower()),
        ('Powhatan, authentication required',
         lambda line: line.startswith('https://powhatan.iiie.disa.mil/')),
        ('IASE COP, authentication required',
         lambda line: line.startswith('https://disa.deps.mil/ext/cop/iase')),
    )
    # we will not get any separate errors, nor non-zero exit codes: if
    # anything goes wrong the output will just be empty.
    for line in p.stdout:
        for reason, clbl in reject_if:
            if clbl(line):
                log.info('skipping %s, because - %s', line, reason)
                break
        else:
            yield line.strip()
    p.stdout.close()
    p.wait()


def main():
    handler = setup_logging()
    log_queue = multiprocessing.Queue(-1)
    log_listener = QueueListener(log_queue, handler)
    log_listener.start()
    log = logging.getLogger('main')
    log.debug('test debug message')
    log.error('test error message')
    time_to_die = time.time() + MAX_LIFETIME
    make_internal_metadata(sparql_url, data_post_url)
    fetch_queue = multiprocessing.JoinableQueue()
    fetcher_count = 1
    fetchers = [multiprocessing.Process(
            target=fetch_process,
            args=(fetch_queue, log_queue, sparql_url, data_post_url))
                for x in range(fetcher_count)]
    for f in fetchers:
        f.start()
    fetch_queue.put('https://nvd.nist.gov/static/feeds/xml/sp80053/rev4/800-53-controls.xml')
    fetch_queue.put('http://iasecontent.disa.mil/stigs/zip/u_cci_list.zip')
    stig_count = 0
    for url in get_stig_urls():
        fetch_queue.put(url)
        stig_count += 1
    log.info('%d STIGs placed on fetch queue', stig_count)
    ingest_quit_queue = multiprocessing.Queue()
    ingestor = multiprocessing.Process(target=ingest_process, args=(
            ingest_quit_queue, log_queue, sparql_url, data_post_url))
    ingestor.start()
    log.debug('ingestor started')
    while (time.time() < time_to_die) and not fetch_queue.empty():
        log.debug('waiting for fetch queue to empty')
        time.sleep(10)
    fetch_queue.close()
    time.sleep(5)
    log.debug('terminating fetcher processes')
    for f in fetchers:
        if f.is_alive():
            f.terminate()
    # now we have fetched everything. let's induct whatever is new
    # from the induct_dir
    log.debug('inducting files from %s', induct_dir)
    for fn in os.listdir(induct_dir):
        if not fn.startswith('.') and not fn.endswith('~'):
            fpn = os.path.join(induct_dir, fn)
            log.debug('inducting file %r', fpn)
            induct(log, fpn, sparql_url, data_post_url)
    while(time.time() < time_to_die) and ingestor.is_alive():
        log.debug('allowing time for ingestor until time_to_die')
        time.sleep(60)
    ingest_quit_queue.put('like tears in rain')
    time.sleep(5)
    for c in multiprocessing.active_children():
        log.error('at exit, this child process was still running: %r', c)
        c.terminate()
    log_listener.stop()

if __name__ == '__main__':
    main()
