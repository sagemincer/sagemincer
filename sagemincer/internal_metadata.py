# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sagemincer import PROV, SAGEPROV, Xsm, Xsmv, data_version_uri
from sagemincer import raw_hash, safen_hash, hash_property
from sagemincer.to_4store import post_to_graph_turtle
from rdflib import Graph, RDF, Namespace
from rdflib.term import URIRef, Literal
from sagemincer.environment import download_cache_dir
from sagemincer.fs_sparql import SPARQLWrapper, JSON
import uuid
import pkg_resources

TRANSFORMS = Namespace(Xsm.transforms + '/')

class MultipleNewestVersions(Exception):
    pass

def make_internal_metadata(sparql_url, data_post_url):
    make_code_metadata(sparql_url, data_post_url)
    make_transform_metadata(sparql_url, data_post_url)

def make_code_metadata(sparql_url, data_post_url):
    ns = Namespace(data_version_uri + '/')
    versions_query = SPARQLWrapper(sparql_url)
    versions_query.setQuery(
        '''
        PREFIX sageprov: {sageprov_uri}
        PREFIX prov: {prov_uri}
        SELECT ?dv ?newer WHERE {{
               ?dv a sageprov:DataHandlingCode .
               OPTIONAL {{ ?newer prov:wasRevisionOf ?dv }} .
        }}'''.format(
            sageprov_uri=URIRef(SAGEPROV).n3(),
            prov_uri=URIRef(PROV).n3()))
    versions_query.setReturnFormat(JSON)
    versions_results = versions_query.query().convert()
    has_no_newer = [res['dv']['value'] 
                    for res in versions_results['results']['bindings']
                    if len(res['newer']) == 0]
    if len(has_no_newer) == 0:
        previous_newest_version = None
    elif len(has_no_newer) == 1:
        previous_newest_version = URIRef(has_no_newer[0])
    else:
        raise MultipleNewestVersions(has_no_newer)
    if data_version_uri == previous_newest_version:
        # we are the latest version. do nothing.
        pass
    else:
        g = Graph()
        g.add((data_version_uri, RDF.type, SAGEPROV.DataHandlingCode))
        g.add((data_version_uri, RDF.type, PROV.Entity))
        if previous_newest_version is not None:
            g.add((data_version_uri, PROV.wasRevisionOf, previous_newest_version))
        post_to_graph_turtle(data_post_url, Xsmv.graph, g)


def transforms():
    for name in pkg_resources.resource_listdir(
        'sagemincer', 'transforms'):
        if name.endswith('.xsl'):
            checksum = safen_hash(raw_hash(pkg_resources.resource_stream(
                'sagemincer', 'transforms/' + name).read()))
            uri = URIRef(TRANSFORMS[checksum])
            yield (name, checksum, uri)

def make_transform_metadata(sparql_url, data_post_url):
    g = Graph()
    for name, checksum, uri in transforms():
        exists_query = SPARQLWrapper(sparql_url)
        exists_query.setQuery(
            '''
            PREFIX prov: {prov_uri}
            ASK {{ {uri} a prov:Entity }}
            '''.format(uri=uri.n3(), prov_uri=URIRef(PROV).n3()))
        exists_query.setReturnFormat(JSON)
        exists_results = exists_query.query().convert()
        exists = exists_results['boolean']
        if not exists:
            g.add((uri, RDF.type, PROV.Entity))
            g.add((uri, RDF.type, SAGEPROV.File))
            g.add((uri, RDF.type, SAGEPROV.Transform))
            g.add((uri, hash_property, Literal(checksum)))
    post_to_graph_turtle(data_post_url, TRANSFORMS.graph, g)


def get_transform(transform_filename):
    """Get a transform file from inside sagemincer; return its stream and URI.
    """
    get_stream = lambda: pkg_resources.resource_stream(
        'sagemincer', 'transforms/' + transform_filename)
    checksum = safen_hash(raw_hash(get_stream().read()))
    uri = URIRef(TRANSFORMS[checksum])
    # we get the stream again, rather than depending on its rewindability.
    return get_stream(), uri
