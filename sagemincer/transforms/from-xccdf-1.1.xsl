<?xml version="1.0" ?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:cdf="http://checklists.nist.gov/xccdf/1.1"
		xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
		xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
		xmlns:xccdflod="http://securityrules.info/ns/xccdflod/1#">
<!--
sagemincer - expose security checklists as Linked Open Data
Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->


  <xsl:param name="stig_uri" />
  <!-- The cci_base_uri should end with a slash, because we will concat a
       Control Correlation Identifier (CCI) onto it. -->
  <xsl:param name="cci_base_uri" />

  <xsl:output method="xml" indent="yes" />

  <xsl:template match="/">
    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
      <!-- If the stig_uri does not end with a slash, and we use it verbatim as
           the xml:base, then when we say rdf:about="foo" under that value of
           xml:base, we'll end up referring to http://example.org/id/cl/foo
           rather than http://example.org/id/cl/32r8h2823rh/foo -->
      <xsl:attribute name="xml:base">
        <xsl:choose>
          <xsl:when test="substring($stig_uri, string-length($stig_uri), 1) = '/'">
            <xsl:value-of select="$stig_uri" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat($stig_uri, '/')" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates />
    </rdf:RDF>
  </xsl:template>

  <xsl:template match="cdf:Benchmark">
    <!-- make a link from the stig to this Benchmark -->
    <!-- The end of the URI identifying the Benchmark should likely be
         {@id}, not "top", but as of early 2014, 36 DoD STIGs have id values
         for their Benchmark elements which are not usable because they aren't
         valid NCNames
         <http://www.w3.org/TR/1999/REC-xml-names-19990114/#NT-NCName>. -->
    <rdf:Description rdf:about="{$stig_uri}">
      <xccdflod:hasToplevel rdf:resource="top" />
    </rdf:Description>
    <xccdflod:Benchmark rdf:about="top">
      <!-- We are going to act on the status element with the latest
           date. -->
      <xsl:for-each select="cdf:status">
	<!-- BRITTLE: xsd:dates are ISO-8601 IIRC, which allows for
	     dashes (1970-01-01) or not (19700101). If both are used
	     in the same document, the text-based sort will fail. XSLT
	     1.0 does not appear to include a "date" data-type for
	     xsl:sort. -->
	<xsl:sort select="@date" data-type="text" order="descending" />
	<xsl:if test="position() = 1">
	  <xccdflod:latestStatus
	      ><xsl:copy-of select="text()|./*"/></xccdflod:latestStatus>
	  <xccdflod:latestDate rdf:datatype="http://www.w3.org/2001/XMLSchema#date"
			 ><xsl:value-of select="@date"/></xccdflod:latestDate>
	</xsl:if>
      </xsl:for-each>
      <xsl:apply-templates/>
    </xccdflod:Benchmark>
  </xsl:template>

  <xsl:template match="cdf:title">
    <rdfs:label><xsl:value-of select="."/></rdfs:label>
  </xsl:template>

  <!-- We are not matching all description tags here, because in DoD STIGs the
       profile, group and rule descriptions contain escaped custom XML, which
       we want to get stuff out of, and it's not worth anything to store the
       content taken as a string. So a separate stylesheet specific to STIGs
       will pull the information out of item description elements. -->
  <xsl:template match="cdf:Benchmark/cdf:description">
    <rdfs:comment rdf:parseType="literal">
      <xsl:copy-of select="text()|./*"/>
    </rdfs:comment>
  </xsl:template>

  <!-- If there was any good info in the id attribute, we lose it here -->
  <xsl:template match="cdf:notice">
    <xccdflod:notice rdf:parseType="literal">
      <xsl:copy-of select="text()|./*"/>
    </xccdflod:notice>
  </xsl:template>

  <xsl:template match="cdf:front-matter">
    <xccdflod:frontMatter rdf:parseType="literal">
      <xsl:copy-of select="text()|./*" />
    </xccdflod:frontMatter>
  </xsl:template>

  <xsl:template match="cdf:rear-matter">
    <xccdflod:rearMatter rdf:parseType="literal">
      <xsl:copy-of select="text()|./*" />
    </xccdflod:rearMatter>
  </xsl:template>

  <!-- FIXME we do not deal with xccdf:subs which are what plain-texts are
       meant for.
       -->
  <xsl:template match="cdf:plain-text" />

  <!-- Versions are supposed to have dates and possibly URIs
       associated with them. We lose those things here, and to keep
       them would mean qualifying the version (thebenchmark :version [
       a :Version; :value <xsl:value-of select="."/>; :date
       <xsl:value-of select="@date"/>; etc ] .). But STIGs don't have
       any attributes on their version elements, so we don't worry
       about that yet.
  -->
  <xsl:template match="cdf:version">
    <xccdflod:version><xsl:value-of select="."/></xccdflod:version>
  </xsl:template>

  <!-- The Item is abstract. We call its template from those of its concrete
       heritors. -->
  <xsl:template name="cdf:Item">
    <!-- FIXME deal with abstract, cluster-id, extends, prohibitChanges
         attributes -->
    <xsl:if test="@hidden">
      <xccdflod:hidden rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean"
      ><xsl:value-of select="@hidden" /></xccdflod:hidden>
    </xsl:if>
  </xsl:template>

  <!-- So also the SelectableItem -->
  <xsl:template name="cdf:SelectableItem">
    <xsl:call-template name="cdf:Item" />
    <xsl:if test="@selected">
      <xccdflod:selected rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean"
      ><xsl:value-of select="@selected" /></xccdflod:selected>
    </xsl:if>
    <xsl:if test="@severity">
      <xccdflod:severity><xsl:value-of select="@severity" /></xccdflod:severity>
    </xsl:if>
  </xsl:template>

  <xsl:template match="cdf:Value">
    <xccdflod:hasValue>
      <xccdflod:Value rdf:about="{@id}">
        <xsl:apply-templates/>
      </xccdflod:Value>
    </xccdflod:hasValue>
  </xsl:template>

  <xsl:template match="cdf:Profile">
    <xccdflod:hasProfile>
      <xccdflod:Profile rdf:about="{@id}">
        <xsl:apply-templates/>
        <xsl:for-each select="cdf:select[@selected='true']">
          <xccdflod:contains rdf:resource="{@idref}"/>
        </xsl:for-each>
      </xccdflod:Profile>
    </xccdflod:hasProfile>
  </xsl:template>
  
  <xsl:template match="cdf:Group">
    <xccdflod:hasGroup>
      <xccdflod:Group rdf:about="{@id}">
        <xsl:call-template name="cdf:SelectableItem" />
	<xsl:apply-templates/>
      </xccdflod:Group>
    </xccdflod:hasGroup>
  </xsl:template>

  <xsl:template match="cdf:Rule">
    <xccdflod:hasRule>
      <xccdflod:Rule about="{@id}">
        <xsl:call-template name="cdf:SelectableItem" />
        <xsl:apply-templates/>
      </xccdflod:Rule>
    </xccdflod:hasRule>
  </xsl:template>

  <xsl:template match="cdf:ident">
    <xsl:choose>
      <xsl:when test="@system = 'http://iase.disa.mil/cci'">
        <!-- The ident element has important semantic meaning, but we'll write
             that somewhere else, not here. -->
        <xccdflod:ident rdf:resource="{concat($cci_base_uri, .)}" />
      </xsl:when>
      <!-- FIXME support more system identifiers -->
    </xsl:choose>
  </xsl:template>

  <xsl:template match="cdf:fixtext">
    <xccdflod:fixText>
      <xsl:copy-of select="text()|./*"/>
    </xccdflod:fixText>
  </xsl:template>

  <xsl:template match="cdf:check/cdf:check-content">
    <xccdflod:checkContent>
      <xsl:copy-of select="text()|./*"/>
    </xccdflod:checkContent>
  </xsl:template>

  <!-- ignore text not inside elements we are paying attention to -->
  <xsl:template match="text()" />

  <!-- FIXME below elements are not dealt with at all, so we give them empty
       templates to limit unexpected effects -->
  <xsl:template match="cdf:reference" />
  <xsl:template match="cdf:metadata" />
  <xsl:template match="cdf:model" />
  <xsl:template match="cdf:TestResult" />
  <xsl:template match="cdf:signature" />
  <!-- cdf:status is handled above in cdf:Benchmark -->
  <xsl:template match="cdf:warning" />
  <xsl:template match="cdf:question" />
  <xsl:template match="cdf:rationale" />
  <xsl:template match="cdf:platform" />
  <xsl:template match="cdf:requires" />
  <xsl:template match="cdf:conflicts" />

</xsl:stylesheet>
