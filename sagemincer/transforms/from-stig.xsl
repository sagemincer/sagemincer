<?xml version="1.0" ?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:cdf="http://checklists.nist.gov/xccdf/1.1"
		xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
		xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
		xmlns:xccdflod="http://securityrules.info/ns/xccdflod/1#"
                xmlns:stiglod="http://securityrules.info/ns/stiglod/1#"
                xmlns:xhtml="http://www.w3.org/1999/xhtml">
<!--
sagemincer - expose security checklists as Linked Open Data
Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->


  <xsl:param name="stig_uri" />

  <xsl:output method="xml" indent="yes" />

  <xsl:template match="/">
    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
      <!-- If the stig_uri does not end with a slash, and we use it verbatim as
           the xml:base, then when we say rdf:about="foo" under that value of
           xml:base, we'll end up referring to http://example.org/id/cl/foo
           rather than http://example.org/id/cl/32r8h2823rh/foo -->
      <xsl:attribute name="xml:base">
        <xsl:choose>
          <xsl:when test="substring($stig_uri, string-length($stig_uri), 1) = '/'">
            <xsl:value-of select="$stig_uri" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat($stig_uri, '/')" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates />
    </rdf:RDF>
  </xsl:template>

  
  <xsl:template match="cdf:plain-text[@id='release-info']">
    <rdf:Description rdf:about="top">
      <stiglod:releaseInfo><xsl:value-of select="." /></stiglod:releaseInfo>
      <xsl:if test="contains(text(), 'Release: ')">
	<stiglod:releaseNumber
	    ><xsl:value-of select="substring-before(
	    substring-after(text(), 'Release: '), ' ')"/></stiglod:releaseNumber>
      </xsl:if>
      <!-- FIXME? We drop the "Benchmark Date" on the floor. There's a
           status tag isn't there? Why do we need another date, which
           by the way isn't in ISO-8601 format and we are not going to
           even try to transmogrify it using XSLT 1.0? Feh. -->
    </rdf:Description>
  </xsl:template>

  <!--
  When we are tokenizing by commas, and we are inside a
  cdf:Rule/cdf:description, here's what comes out. We can't distinguish what
  pseudo-element we're dealing with here; fortunately there's only one of
  interest that has comma-separated values to tokenize.
  -->
  <xsl:template match="cdf:Rule/cdf:description" mode="tokenize-by-commas">
    <xsl:param name="token" />
    <stiglod:iaControl><xsl:value-of select="$token" /></stiglod:iaControl>
  </xsl:template>

  <xsl:template match="cdf:Group/cdf:description|cdf:Rule/cdf:description">
    <rdf:Description rdf:about="{../@id}">
      <xsl:call-template name="pseudoelement-contents-to-boolean-property-value">
        <xsl:with-param name="pseudo-tag" select="'Documentable'" />
        <xsl:with-param name="property-tag" select="'stiglod:documentable'" />
      </xsl:call-template>
      <xsl:call-template name="pseudoelement-contents-to-xhtml-property-value">
        <xsl:with-param name="pseudo-tag" select="'FalseNegatives'" />
        <xsl:with-param name="property-tag" select="'stiglod:falseNegatives'" />
      </xsl:call-template>
      <xsl:call-template name="pseudoelement-contents-to-xhtml-property-value">
        <xsl:with-param name="pseudo-tag" select="'FalsePositives'" />
        <xsl:with-param name="property-tag" select="'stiglod:falsePositives'" />
      </xsl:call-template>
      <xsl:call-template name="pseudoelement-contents-to-xhtml-property-value">
        <xsl:with-param name="pseudo-tag" select="'GroupDescription'" />
        <xsl:with-param name="property-tag" select="'stiglod:groupDescription'" />
      </xsl:call-template>

      <!-- This one has comma-delimited multiple values. -->
      <xsl:call-template name="pseudoelement-comma-delimited-stripped-multiple-values">
        <xsl:with-param name="pseudo-tag" select="'IAControls'" />
        <xsl:with-param name="property-tag" select="'stiglod:iaControl'" />
      </xsl:call-template>

      <xsl:call-template name="pseudoelement-contents-to-xhtml-property-value">
        <xsl:with-param name="pseudo-tag" select="'MitigationControl'" />
        <xsl:with-param name="property-tag" select="'stiglod:mitigationControl'" />
      </xsl:call-template>
      <xsl:call-template name="pseudoelement-contents-to-xhtml-property-value">
        <xsl:with-param name="pseudo-tag" select="'Mitigations'" />
        <xsl:with-param name="property-tag" select="'stiglod:mitigations'" />
      </xsl:call-template>
      <xsl:call-template name="pseudoelement-contents-to-xhtml-property-value">
        <xsl:with-param name="pseudo-tag" select="'PotentialImpacts'" />
        <xsl:with-param name="property-tag" select="'stiglod:potentialImpacts'" />
      </xsl:call-template>
      <xsl:call-template name="pseudoelement-contents-to-xhtml-property-value">
        <xsl:with-param name="pseudo-tag" select="'ProfileDescription'" />
        <xsl:with-param name="property-tag" select="'stiglod:profileDescription'" />
      </xsl:call-template>
      <xsl:call-template name="pseudoelement-contents-to-xhtml-property-value">
        <xsl:with-param name="pseudo-tag" select="'Responsibility'" />
        <xsl:with-param name="property-tag" select="'stiglod:responsibility'" />
      </xsl:call-template>
      <xsl:call-template name="pseudoelement-contents-to-xhtml-property-value">
        <xsl:with-param name="pseudo-tag" select="'SeverityOverrideGuidance'" />
        <xsl:with-param name="property-tag" select="'stiglod:severityOverrideGuidance'" />
      </xsl:call-template>
      <xsl:call-template name="pseudoelement-contents-to-xhtml-property-value">
        <xsl:with-param name="pseudo-tag" select="'ThirdPartyTools'" />
        <xsl:with-param name="property-tag" select="'stiglod:thirdPartyTools'" />
      </xsl:call-template>
      <xsl:call-template name="pseudoelement-contents-to-xhtml-property-value">
        <xsl:with-param name="pseudo-tag" select="'VulnDiscussion'" />
        <xsl:with-param name="property-tag" select="'stiglod:vulnDiscussion'" />
      </xsl:call-template>
    </rdf:Description>
  </xsl:template>

  <xsl:template match="text()" />

  <!-- 
  Find the contents of the pseudoelement with tag $pseudo-tag, and output an
  element with tag $property-tag containing those contents. If the
  pseudoelement is empty, output nothing.
  -->
  <xsl:template name="pseudoelement-contents-to-xhtml-property-value">
    <xsl:param name="pseudo-tag" />
    <xsl:param name="property-tag" />
    <xsl:if test="string-length(substring-after(substring-before(.,concat('&lt;/', $pseudo-tag, '&gt;')), concat('&lt;', $pseudo-tag, '&gt;')))>0">
      <xsl:element name="{$property-tag}">
        <xsl:attribute name="rdf:parseType">literal</xsl:attribute>
        <!-- If we were to do this below, our template would run ~16 times
             faster (shaky number based on a single test), but newlines and
             tabs in the XCCDF content would not be converted into <br/>
             elements and {sequences of 5 non-breaking spaces}, respectively.
             -->
        <!-- <xsl:value-of select="substring-after(substring-before(.,concat('&lt;/', $pseudo-tag, '&gt;')), concat('&lt;', $pseudo-tag, '&gt;'))" /> -->

        <!-- This runs way slower, but converts carriage returns and tabs. It
             is due to the folks at DISA FSO. -->
        <xsl:call-template name="buffer-overflow">
          <xsl:with-param name="string-size" select="string-length(substring-after(substring-before(.,concat('&lt;/', $pseudo-tag, '&gt;')), concat('&lt;', $pseudo-tag, '&gt;')))"/>
          <xsl:with-param name="string-target" select="substring-after(substring-before(.,concat('&lt;/', $pseudo-tag, '&gt;')),
                                                                                        concat('&lt;', $pseudo-tag, '&gt;'))"/>
        </xsl:call-template>
      </xsl:element>
    </xsl:if>
  </xsl:template>

  <xsl:template name="pseudoelement-comma-delimited-stripped-multiple-values">
    <xsl:param name="pseudo-tag" />
    <xsl:param name="property-tag" />
    <xsl:variable name="contents">
      <xsl:call-template name="pseudoelement-contents-or-empty-string">
        <xsl:with-param name="pseudo-tag" select="$pseudo-tag" />
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="tokenize-by-commas">
      <xsl:with-param name="string" select="$contents" />
      <xsl:with-param name="out-element" select="'stiglod:iaControl'" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="pseudoelement-contents-to-boolean-property-value">
    <xsl:param name="pseudo-tag" />
    <xsl:param name="property-tag" />
    <xsl:variable name="contents">
      <xsl:call-template name="pseudoelement-contents-or-empty-string">
        <xsl:with-param name="pseudo-tag" select="$pseudo-tag" />
      </xsl:call-template>
    </xsl:variable>
    <!--
    ASSUMPTION: if the contents exist, they will be a valid XSD boolean
    literal. In 2014 this was true over the entire quarterly STIG library.
    -->
    <xsl:if test="normalize-space($contents) != ''">
      <xsl:element name="{$property-tag}">
        <xsl:attribute name="rdf:datatype"
        >http://www.w3.org/2001/XMLSchema#boolean</xsl:attribute>
        <xsl:value-of select="$contents" />
      </xsl:element>
    </xsl:if>
  </xsl:template>

  <xsl:template name="pseudoelement-contents-or-empty-string">
    <xsl:param name="pseudo-tag" />
    <xsl:if test="string-length(substring-after(substring-before(.,concat('&lt;/', $pseudo-tag, '&gt;')), concat('&lt;', $pseudo-tag, '&gt;')))>0">
      <xsl:value-of select="substring-after(substring-before(.,concat('&lt;/', $pseudo-tag, '&gt;')), concat('&lt;', $pseudo-tag, '&gt;'))" />
    </xsl:if>
  </xsl:template>

  <!--
  For each comma-separated value X in the string parameter, apply the template
  for the mode "tokenize-by-commas" that matches where we are in the document,
  passing X as the parameter "token." Values are run through the
  normalize-space function, which removes leading and trailing spaces, and
  replaces multiple spaces with a single space.
  -->
  <xsl:template name="tokenize-by-commas">
    <!-- We expect that the string will have length less than, say, 200. -->
    <xsl:param name="string" />
    <xsl:param name="out-element" />
    <xsl:param name="this-token" value="" />
    <xsl:choose>
      <xsl:when test="string-length($string) = 0">
        <xsl:if test="string-length($this-token) &gt; 0">

          <!-- finish up. - output! -->
          <xsl:apply-templates select="." mode="tokenize-by-commas">
            <xsl:with-param name="token" select="normalize-space($this-token)" />
          </xsl:apply-templates>

        </xsl:if>
      </xsl:when>
      <xsl:when test="substring($string, 1, 1) = ','">

        <!-- this token is over. - output! -->
        <xsl:apply-templates select="." mode="tokenize-by-commas">
          <xsl:with-param name="token" select="normalize-space($this-token)" />
        </xsl:apply-templates>

        <xsl:call-template name="tokenize-by-commas">
          <xsl:with-param name="string" 
                          select="substring($string, 2,
                                            string-length($string)-1)" />
          <xsl:with-param name="out-element" select="$out-element" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="tokenize-by-commas">
          <xsl:with-param name="string" 
                          select="substring($string, 2,
                                            string-length($string)-1)" />
          <xsl:with-param name="out-element" select="$out-element" />
          <xsl:with-param name="this-token"
                          select="concat($this-token,
                                         substring($string, 1, 1))" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



<!-- The following large passage is from STIG_unclass.xsl as found in DoD
       STIG distributions in 2014 at <http://iase.disa.mil>. Being a work of
       the U.S. Government, it is not subject to U.S. copyright. -->
<!-- Enhancement: br elements are given the xhtml namespace. -->

  <!-- *************************************************************************************************************************************-->
  <!--       The following subroutine performs a transformation that preserves all the line feeds and tabs found                    -->
  <!--       in the original XML file thus making the text more human-readable                                                                  -->
  <!--       It changes all line feeds to <br/>                                                                                                                  -->
  <!--       It changes all tabs to 5 spaces, that is, &#160; &#160; &#160; &#160; &#160;                                                -->
  <!-- *************************************************************************************************************************************-->
  <xsl:template name="this-is-a-subroutine">
    <xsl:param name="string-size"/>
    <xsl:param name="string-target"/>
    <xsl:param name="index"/>
    <!-- <b>This is a subroutine</b><br/> -->
    <!-- <b>This is the text length:</b>&#160; -->
    <!-- <xsl:value-of select="$string-size"/><br/> -->
    <!-- <b>This is the index:</b>&#160; -->
    <!--  <xsl:value-of select="$index"/><br/> -->
    <xsl:if test="$index &lt;= $string-size and $string-size &gt;= 0">
      <!--<b>String is:&#160;"</b>-->
      <!-- <xsl:value-of select="substring($string-target,$index,1)"/>    -->
      <xsl:choose>
        <xsl:when test="substring($string-target,$index,1) = '&#13;' ">
          <!-- Convert line feed to <br/>    -->
          <xhtml:br/>
        </xsl:when>
        <xsl:when test="substring($string-target,$index,1) = '&#10;' ">
          <!-- Convert new line to <br/>     -->
          <xhtml:br/>
        </xsl:when>
        <xsl:when test="substring($string-target,$index,1) = '&#9;' "><!-- Convert tab to 5 spaces      -->
  &#xA0;&#xA0;&#xA0;&#xA0;&#xA0;
  </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="substring($string-target,$index,1)"/>
          <!-- Otherwise, print out charcter as it is -->
        </xsl:otherwise>
      </xsl:choose>
      <xsl:call-template name="this-is-a-subroutine">
        <xsl:with-param name="string-size" select="$string-size"/>
        <xsl:with-param name="string-target" select="$string-target"/>
        <xsl:with-param name="index" select="$index + 1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  <!-- *************************************************************************************************************************************-->
  <!--       The following subroutine prevents recursive overflow for fields up to 11,000 characters long                               -->
  <!--       If a field is greater than 11,000 characters, the program will write out the first 11,000 characters.                      -->
  <!--                                                                                                                                                                       -->
  <!--       If string is from 1 to 1,000 characters long then;                                                                                            -->
  <!--             1. Print out all the characters, flush buffer, return                                                                                    -->
  <!--                                                                                                                                                                       -->
  <!--       Else if string is 1,001 to 2,000 characters long then;                                                                                    -->
  <!--            1. print out 1st 1,000 characters, flush buffer                                                                                          -->
  <!--            2. print out characters 1,001 to string-length, flush buffer, return                                                               -->
  <!--                                                                                                                                                                      -->
  <!--       Else if string is 2,001 - 3,000 characters long then;                                                                                      -->
  <!--            1. print out 1st 1,000 characters, flush buffer                                                                                          -->
  <!--            2. print out characters 1,001 to 2,000, flush buffer                                                                                  -->
  <!--            3. print out characters 2,001 to string length, return                                                                               -->
  <!--                                                                                                                                                                     -->
  <!--                                                                                                                                                                      -->
  <!--      Else                                                                                                                                                          -->
  <!--                             ************                                                                                                                          -->
  <!--      Else                                                                                                                                                          -->
  <!--                             ************                                                                                                                          -->
  <!--      Else                                                                                                                                                         -->
  <!--                             ************                                                                                                                          -->
  <!--       Else if string is 10,001 - 11,000 characters long then;                                                                                    -->
  <!--            1. print out 1st 1,000 characters, flush buffer                                                                                          -->
  <!--            2. print out characters 1,001 to 2,000, flush buffer                                                                                  -->
  <!--                             *************                                                                                                                        -->
  <!--            9. print out characters  9,001 to 10,000, flush buffer                                                                                -->
  <!--           10. print out characters 10,001 to string length, return                                                                              -->
  <!--                                                                                                                                                                     -->
  <!--       Else if string is > 11,000 then;                                                                                                                  -->
  <!--           1. print out 1st 1,000 characters, flush buffer                                                                                          -->
  <!--           2. print out characters 1,001 to 2,000, flush buffer                                                                                  -->
  <!--                                                                                                                                                                    -->
  <!--           9. print out characters 9,001 to 10,000, flush buffer, return                                                                      -->
  <!--          10. print out characters 10,001 to 11,000, flush buffer, return                                                                    -->
  <!--          11. all characters beyond 11,000 will be ignored                                                                                      -->
  <!--                                                                                                                                                                    -->
  <!--***********************************************************************************************************************************-->
  <!--                                                                                                                                                                   -->
  <xsl:template name="buffer-overflow">
    <xsl:param name="string-size"/>
    <xsl:param name="string-target"/>
    <!-- String size:&#160;<xsl:value-of select="$string-size"></xsl:value-of>                                  -->
    <xsl:choose>
      <!-- string is 2 to 1,000 characters in length -->
      <xsl:when test="$string-size &gt; 1 and $string-size &lt;= 1000">
        <!-- print out characters #1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size"/>
          <xsl:with-param name="string-target" select="$string-target"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is 1,001 to 2,000 characters in length -->
      <xsl:when test="$string-size &gt; 1000 and $string-size &lt;= 2000">
        <!-- print out characters #1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters #1001 through end of string -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,$string-size - 1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is 2,001 to 3,000 characters in length -->
      <xsl:when test="$string-size &gt; 2000 and $string-size &lt;= 3000">
        <!-- print out characters # 1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 through 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through end of string -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 2000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001, $string-size - 2000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is 3,001 to 4,000 characters in length -->
      <xsl:when test="$string-size &gt; 3000 and $string-size &lt;= 4000">
        <!-- print out characters # 1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 through 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through end of string -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 3000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001, $string-size - 3000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is 4,001 - 5,000 characters in length -->
      <xsl:when test="$string-size &gt; 4000 and $string-size &lt;= 5000">
        <!-- print out characters # 1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 through 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through end of string -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 4000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001, $string-size - 4000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is 5,001 - 6,000 characters in length -->
      <xsl:when test="$string-size &gt; 5000 and $string-size &lt;= 6000">
        <!-- print out characters # 1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 through 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through 5,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 5,001 through end of string -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 5000"/>
          <xsl:with-param name="string-target" select="substring($string-target,5001, $string-size - 5000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is 6,001 - 7,000 characters in length -->
      <xsl:when test="$string-size &gt; 6000 and $string-size &lt;= 7000">
        <!-- print out characters # 1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 through 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through 5,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 5,001 through 6,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,5001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 6,001 through end of string -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 6000"/>
          <xsl:with-param name="string-target" select="substring($string-target,6001, $string-size - 6000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is 7,001 - 8,000 characters in length -->
      <xsl:when test="$string-size &gt; 7000 and $string-size &lt;= 8000">
        <!-- print out characters # 1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 through 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through 5,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 5,001 through 6,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,5001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 6,001 through 7,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,6001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 7,001 through end of string -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 7000"/>
          <xsl:with-param name="string-target" select="substring($string-target,7001, $string-size - 7000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is 8,001 - 9,000 characters in length -->
      <xsl:when test="$string-size &gt; 8000 and $string-size &lt;= 9000">
        <!-- print out characters # 1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 through 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through 5,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 5,001 through 6,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,5001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 6,001 through 7,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,6001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 7,001 through 8,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,7001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 8,001 through end of string -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 8000"/>
          <xsl:with-param name="string-target" select="substring($string-target,8001, $string-size - 8000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is between 9,001 - 10,000 characters in length -->
      <xsl:when test="$string-size &gt; 9000 and $string-size &lt;= 10000">
        <!-- print out characters # 1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 through 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through 5,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 5,001 through 6,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,5001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 6,001 through 7,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,6001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 7,001 through 8,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,7001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 8,001 through 9,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,8001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 9,001 through end of string -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 9000"/>
          <xsl:with-param name="string-target" select="substring($string-target,9001, $string-size - 9000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is between 10,001 - 11,000 characters in length -->
      <xsl:when test="$string-size &gt; 10000 and $string-size &lt;= 11000">
        <!-- print out characters # 1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 through 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through 5,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 5,001 through 6,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,5001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 6,001 through 7,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,6001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 7,001 through 8,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,7001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 8,001 through 9,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,8001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 9,001 through 10,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,9001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 10,001 through end of string -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 10000"/>
          <xsl:with-param name="string-target" select="substring($string-target,10001, $string-size - 10000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is between 11,001 - 12,000 characters in length -->
      <xsl:when test="$string-size &gt; 11000 and $string-size &lt;= 12000">
        <!-- print out characters # 1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 through 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through 5,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 5,001 through 6,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,5001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 6,001 through 7,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,6001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 7,001 through 8,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,7001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 8,001 through 9,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,8001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 9,001 through 10,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,9001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 10,001 through 11,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 10000"/>
          <xsl:with-param name="string-target" select="substring($string-target,10001, $string-size - 10000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 11,001 through 12,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 10000"/>
          <xsl:with-param name="string-target" select="substring($string-target,11001, $string-size - 10000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is between 12,001 - 13,000 characters in length -->
      <xsl:when test="$string-size &gt; 12000 and $string-size &lt;= 13000">
        <!-- print out characters # 1 through 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 through 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through 5,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 5,001 through 6,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,5001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 6,001 through 7,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,6001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 7,001 through 8,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,7001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 8,001 through 9,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,8001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 9,001 through 10,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,9001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 10,001 through 11,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 10000"/>
          <xsl:with-param name="string-target" select="substring($string-target,10001, $string-size - 10000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 11,001 through 12,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 11000"/>
          <xsl:with-param name="string-target" select="substring($string-target,11001, $string-size - 10000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 12,001 through 13,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="$string-size - 12000"/>
          <xsl:with-param name="string-target" select="substring($string-target,12001, $string-size - 10000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is between 13,001 - 14,000 characters in length -->
      <xsl:when test="$string-size &gt; 13000 and $string-size &lt;= 14000">
        <!-- print out characters # 1 - 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 - 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="index" select="1"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
        </xsl:call-template>
        <xsl:call-template name="this-is-a-subroutine">
          <!-- print out characters # 2,001 through 3,000 -->
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through 5,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 5,001 through 6,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,5001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 6,001 through 7,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,6001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 7,001 through 8,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,7001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 8,001 through 9,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,8001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 9,001 - 10,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,9001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 10,001 - 11,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,10001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 11,001 - 12,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,11001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 12,001 - 13,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,12001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 13,001 - 14,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,13001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is between 14,001 - 15,000 characters in length -->
      <xsl:when test="$string-size &gt; 14000 and $string-size &lt;= 15000">
        <!-- print out characters # 1 - 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 - 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="index" select="1"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through 5,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 5,001 through 6,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,5001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 6,001 through 7,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,6001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 7,001 through 8,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,7001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 8,001 through 9,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,8001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 9,001 - 10,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,9001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 10,001 - 11,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,10001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 11,001 - 12,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,11001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 12,001 - 13,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,12001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 13,001 - 14,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,13001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 14,001 - 15,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,14001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
      <!-- string is greater than 15,000 characters long, truncate all characters > 15,000 -->
      <xsl:when test="$string-size &gt; 15000">
        <!-- print out characters # 1 - 1,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,1,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 1,001 - 2,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="index" select="1"/>
          <xsl:with-param name="string-target" select="substring($string-target,1001,1000)"/>
        </xsl:call-template>
        <!-- print out characters # 2,001 through 3,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,2001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 3,001 through 4,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,3001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 4,001 through 5,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,4001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 5,001 through 6,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,5001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 6,001 through 7,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,6001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 7,001 through 8,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,7001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 8,001 through 9,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,8001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 9,001 - 10,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,9001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 10,001 - 11,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,10001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 11,001 - 12,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,11001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 12,001 - 13,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,12001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 13,001 - 14,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,13001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
        <!-- print out characters # 14,001 - 15,000 -->
        <xsl:call-template name="this-is-a-subroutine">
          <xsl:with-param name="string-size" select="1000"/>
          <xsl:with-param name="string-target" select="substring($string-target,14001,1000)"/>
          <xsl:with-param name="index" select="1"/>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
