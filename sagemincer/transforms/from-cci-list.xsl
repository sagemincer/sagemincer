<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
		xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                xmlns:ccilod="http://securityrules.info/ns/ccilod/1#"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                xmlns:cci="http://iase.disa.mil/cci"
                xmlns:ccifuncs="http://securityrules.info/ns/xslt/sagemincer/ccilist">
<!--
sagemincer - expose security checklists as Linked Open Data
Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->


  <xsl:param name="cci_list_uri" />
  <!-- if the v3_uri is not given, v3 references will not be emitted in the
       output RDF/XML -->
  <xsl:param name="nist_sp_800_53_v3_uri" />
  <xsl:param name="nist_sp_800_53_v4_uri" />

  <xsl:output method="xml" indent="yes" encoding="UTF-8" />

  <xsl:template match="/">
    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
      <!-- If the cci_list_uri does not end with a slash, and we use it verbatim as
           the xml:base, then when we say rdf:about="foo" under that value of
           xml:base, we'll end up referring to http://example.org/id/foo
           rather than http://example.org/id/cci/foo -->
      <xsl:attribute name="xml:base">
        <xsl:choose>
          <xsl:when test="substring($cci_list_uri, string-length($cci_list_uri), 1) = '/'">
            <xsl:value-of select="$cci_list_uri" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat($cci_list_uri, '/')" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <ccilod:CCIList rdf:about="{$cci_list_uri}">
        <xsl:apply-templates />
      </ccilod:CCIList>
    </rdf:RDF>
  </xsl:template>

  <xsl:template match="cci:cci_list">
    <ccilod:hasToplevel>
      <rdf:Description rdf:about="top">
        <xsl:apply-templates />
      </rdf:Description>
    </ccilod:hasToplevel>
  </xsl:template>

  <xsl:template match="metadata" />

  <xsl:template match="cci:cci_item">
    <ccilod:hasItem>
      <ccilod:CCIItem rdf:about="{@id}">
        <rdfs:label><xsl:value-of select="@id" /></rdfs:label>
        <xsl:apply-templates />
      </ccilod:CCIItem>
    </ccilod:hasItem>
  </xsl:template>

  <xsl:template match="cci:status">
    <ccilod:hasStatus><xsl:value-of select="."/></ccilod:hasStatus>
  </xsl:template>

  <xsl:template match="cci:definition">
    <rdfs:comment><xsl:value-of select="." /></rdfs:comment>
  </xsl:template>

  <xsl:template match="cci:references">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="cci:reference[@creator='NIST'
                                     and @title='NIST SP 800-53'
                                     and @version='3']">

  <!-- FIXME? references with commas, ampersands or the word "and" in them
       should result in multiple ccilod:refersTo triples, but are instead
       translated verbatim into a single refersTo which won't point to anything
       sense-making -->
    <xsl:variable name="normalized_index"
                  select="translate(normalize-space(@index),
                                    ' (),*&amp;',
                                    '_LRκαν')" />
    <xsl:if test="string-length($nist_sp_800_53_v3_uri) &gt; 0">
      <ccilod:refersTo rdf:resource="{concat($nist_sp_800_53_v3_uri,
                                             $normalized_index)}" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="cci:reference[@creator='NIST'
                                     and @title='NIST SP 800-53 Revision 4'
                                     and @version='4']">
    <!-- The custom function we call here deals with differences in
         spacing and punctuation between the way the reference is
         written in this CCI list and the way the thing we are
         referring to is written in the NIST SP 800-53 v4 XML. -->
    <xsl:variable name="resolved_uri"
		  select="ccifuncs:find_sp80053v4_requirement(
			  $nist_sp_800_53_v4_uri,
			  normalize-space(@index))" />
    <xsl:choose>
      <xsl:when test="string-length($resolved_uri) &gt; 0">
	<ccilod:refersTo rdf:resource="{$resolved_uri}" />
      </xsl:when>
      <xsl:otherwise>
	<!-- couldn't find it, we'll just have a broken link :( -->
	<xsl:variable name="normalized_index"
                      select="translate(normalize-space(@index),
                              ' (),*&amp;',
                              '_LRκαν')" />
	<ccilod:refersTo rdf:resource="{concat($nist_sp_800_53_v4_uri,
                                       $normalized_index)}" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="text()" />
</xsl:stylesheet>
