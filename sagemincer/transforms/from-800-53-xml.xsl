<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
		xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                xmlns:c="http://scap.nist.gov/schema/sp800-53/2.0"
                xmlns:cf="http://scap.nist.gov/schema/sp800-53/feed/2.0"
                xmlns="http://securityrules.info/ns/sp80053lod/1#">
<!--
sagemincer - expose security checklists as Linked Open Data
Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

  <!-- this has to NOT end with a slash -->
  <xsl:param name="nist_sp_800_53_v4_document_uri" />

  <xsl:output method="xml" indent="yes" encoding="UTF-8" />

  <xsl:template match="/">
    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
      <!-- If the nist_sp_800_53_v4_document_uri does not end with a
           slash, and we use it verbatim as the xml:base, then when we
           say rdf:about="foo" under that value of xml:base, we'll end
           up referring to http://example.org/id/80053/foo rather than
           http://example.org/id/80053/32r8h2823rh/foo -->
      <xsl:attribute name="xml:base">
        <xsl:choose>
          <xsl:when test="substring($nist_sp_800_53_v4_document_uri, string-length(nist_sp_800_53_v4_document_uri), 1) = '/'">
            <xsl:value-of select="$nist_sp_800_53_v4_document_uri" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat($nist_sp_800_53_v4_document_uri, '/')" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <rdf:Description rdf:about="{$nist_sp_800_53_v4_document_uri}">
        <hasToplevel>
          <Controls rdf:about="top">
            <xsl:apply-templates />
          </Controls>
        </hasToplevel>
      </rdf:Description>
    </rdf:RDF>
  </xsl:template>

  <xsl:template match="cf:controls">
    <xsl:apply-templates/>
  </xsl:template>

  <!-- The numbers of the controls contain characters that are not valid
       in NCNames (<http://www.w3.org/TR/xmlschema-2/#NCName>), i.e. spaces and
       parentheses. We turn these into something valid in an NCName. -->
  <xsl:template name="ncnameify_control_number">
    <xsl:param name="number" />
    <xsl:value-of select="translate(normalize-space($number),
                                    ' ()',
                                    '_LR')" />
  </xsl:template>

  <xsl:template match="cf:control">
    <hasControl>
      <Control>
        <xsl:attribute name="rdf:about"
        ><xsl:call-template name="ncnameify_control_number">
          <xsl:with-param name="number" select="c:number" />
         </xsl:call-template></xsl:attribute>
        <!-- The label doesn't have to be an NCName; leave it alone. -->
        <rdfs:label><xsl:value-of select="c:number" /></rdfs:label>
        <xsl:apply-templates/>
      </Control>
    </hasControl>
  </xsl:template>

  <xsl:template match="c:family">
    <!-- SP 800-53 is in English, so it's ok that we have a small-world view of
         how to lowercase something here. Right? -->
    <xsl:variable name="family_id"
                  select="translate(normalize-space(.),
                                    ' ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                                    '_abcdefghijklmnopqrstuvwxyz')" />
    <family rdf:resource="{concat('family/', $family_id)}" />
  </xsl:template>

  <xsl:template match="c:title">
    <rdfs:comment><xsl:value-of select="." /></rdfs:comment>
  </xsl:template>

  <xsl:template match="c:priority">
    <priority rdf:resource="{concat('priority/', .)}" />
  </xsl:template>

  <!-- FIXME -->
  <xsl:template match="c:baseline-impact" />

  <xsl:template match="c:statement">
    <xsl:choose>
      <xsl:when test="c:number">
        <hasStatement>
          <Statement>
            <xsl:call-template name="ncnameify_id_and_add_label" />
            <xsl:call-template name="collect_descriptions_down_to_self" />
          </Statement>
        </hasStatement>
      </xsl:when>
      <xsl:otherwise>
        <description><xsl:value-of select="c:description"/></description>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="c:control-enhancements">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="c:control-enhancement">
    <hasEnhancement>
      <Enhancement>
        <xsl:call-template name="ncnameify_id_and_add_label" />
        <xsl:call-template name="collect_descriptions_down_to_self" />
        <xsl:apply-templates/>
      </Enhancement>
    </hasEnhancement>
  </xsl:template>

  <!-- FIXME -->
  <xsl:template match="c:supplemental-guidance" />

  <xsl:template match="text()" />




  <xsl:template name="ncnameify_id_and_add_label">
    <xsl:attribute name="rdf:about"
    ><xsl:call-template name="ncnameify_control_number">
      <xsl:with-param name="number" select="c:number" />
     </xsl:call-template></xsl:attribute>
    <rdfs:label><xsl:value-of select="c:number" /></rdfs:label>
  </xsl:template>

  <xsl:template name="collect_descriptions_down_to_self">
    <xsl:if test="count(ancestor-or-self::*/c:description) &gt; 0">
      <description>
        <xsl:for-each select="ancestor-or-self::*/c:description">
          <xsl:if test="normalize-space(.)">
            <xsl:value-of select="."/>
            <xsl:value-of select="' '"/>
          </xsl:if>
        </xsl:for-each>
      </description>
    </xsl:if>
  </xsl:template>




</xsl:stylesheet>
