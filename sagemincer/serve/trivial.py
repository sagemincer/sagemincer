#!/usr/bin/env python2
# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import logging

# IMPORTANT: Put any additional includes below this line.  If placed above this
# line, it's possible required libraries won't be in your searchable path
#

import bottle
import urllib2
import os

import sys
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG, format='[%(asctime)s] %(name)s: %(message)s')
logging.getLogger('top').error('logging set up!')

import sagemincer.bottles.top
application = sagemincer.bottles.top.app

if __name__ == '__main__':
    bottle.run(application, host='0.0.0.0', port=8081, debug=True)
