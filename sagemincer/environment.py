#!/usr/bin/python
# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import logging
from xdg.BaseDirectory import xdg_cache_home

LOG_LEVEL = logging.INFO
LOG_FORMAT = '[%(asctime)s] %(name)s: %(message)s'

def setup_logging():
    if os.getenv('OPENSHIFT_LOG_DIR') is not None:
        log_filename = os.path.join(os.getenv('OPENSHIFT_LOG_DIR'), 'python.log')
        handler = logging.StreamHandler(open(log_filename, 'at'))
    else:
        handler = logging.StreamHandler()
    formatter = logging.Formatter(LOG_FORMAT, None)
    handler.setFormatter(formatter)
    root = logging.getLogger()
    root.addHandler(handler)
    root.setLevel(LOG_LEVEL)
    return handler

_4store_host  = os.getenv('OPENSHIFT_4S_IP', os.getenv('FOURSTORE_PORT_8080_TCP_ADDR', 'localhost'))
_4store_port  = os.getenv('OPENSHIFT_4S_PORT', os.getenv('FOURSTORE_PORT_8080_TCP_PORT', '9000'))
sparql_url    = 'http://{host}:{port}/sparql/'.format(host=_4store_host,
                                                      port=_4store_port)
data_put_url  = 'http://{host}:{port}/data/'.format(  host=_4store_host,
                                                      port=_4store_port)
data_post_url = data_put_url
size_url      = 'http://{host}:{port}/status/size/'.format(host=_4store_host,
                                                           port=_4store_port)
_top_cache_dir = os.getenv('OPENSHIFT_DATA_DIR',
                           xdg_cache_home)
download_cache_dir = os.path.join(_top_cache_dir, 'sagemincer')
induct_dir = os.path.join(download_cache_dir, 'manual-uploads')

# URIs for the nouns (checklist and item identifiers, download identifiers,
# about pages) we are concerned with in this application are derived by
# appending things to this root URI, so that links we emit in results will
# point to URLs served by this instance of this application.
resource_root_uri = os.getenv('SAGEMINCER_RESOURCE_ROOT_URI', 'http://securityrules.info')

if 'OPENSHIFT_PJS_DIR' in os.environ:
    phantomjs_executable = os.path.join(os.getenv('OPENSHIFT_PJS_DIR'),
                                        'usr', 'bin', 'phantomjs')
else:
    phantomjs_executable = os.getenv('SAGEMINCER_PHANTOMJS', 'phantomjs')
