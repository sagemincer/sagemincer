# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import re
import requests
import os.path
from sagemincer import (
    PROV, SAGEPROV, Xdownload, Xingest, Xid, SP80053LOD, CCILOD, XCCDFLOD)
from sagemincer import hash_algorithm, raw_hash, shorten_hash, safen_hash, hash_property
from sagemincer import data_version_uri
from sagemincer.to_4store import post_to_graph_turtle, post_to_graph_rdfxml, delete_graph
from rdflib import Graph, RDF, Namespace
from rdflib.term import URIRef, Literal
from sagemincer.environment import download_cache_dir
from sagemincer.fs_sparql import SPARQLWrapper, JSON
from sagemincer.internal_metadata import transforms, get_transform
import uuid
import time
from zipfile import ZipFile
import io
from lxml.etree import XSLT, parse as etparse, FunctionNamespace
import datetime
import sagemincer.find_requirement
from functools import partial

class NoHandler(Exception):
    pass

class NoDocument(Exception):
    pass

class Sentinel(object):
    def __init__(self, name):
        self.name = name
    def __repr__(self):
        return self.name

fti_uningested = Sentinel('fti_uningested')
fti_partially_ingested = Sentinel('fti_partially_ingested')
fti_ingested_by_old_code = Sentinel('fti_ingested_by_old_code')
fti_used_old_downloaded_file = Sentinel('fti_used_old_downloaded_file')
fti_ingested_with_old_transforms = Sentinel('fti_ingested_with_old_transforms')

def files_to_ingest(sparql_url):
    """For each DownloadedFile, find out about ingests done; yield info.

    There is no guarantee that any DownloadedFile will only be yielded
    once, nor that all ingests about one DownloadedFile will be
    yielded in a group.

    Each thing yielded is a tuple (e, a, d) where e is one of the
    fti_* constants in this module; a is the URI of an Ingest
    Activity; and d is the URI of a DownloadedFile that the Ingest
    used.

    The results of this function are useful for (1) deleting previous
    ingests and their results; and (2) collecting a list of
    DownloadedFiles that should be reingested.

    """
    # This was once going to be multiple functions each with a SELECT
    # ... FILTER NOT EXIST ... query, but that construct is only in
    # SPARQL 1.1 and 4store does SPARQL 1.0
    log = logging.getLogger('files_to_ingest')
    uningested_query = SPARQLWrapper(sparql_url)
    uningested_query.setQuery(
        '''
        PREFIX sageprov: {sageprov_uri}
        PREFIX prov: {prov_uri}
        SELECT DISTINCT ?df ?act ?fin ?code ?dep ?newerdep WHERE {{
            ?df a sageprov:ObtainedFile .
            OPTIONAL {{
                ?act a sageprov:Ingest .
                ?act prov:used ?df .
                OPTIONAL {{
                    ?act prov:endedAtTime ?fin .
                }}
                OPTIONAL {{
                    ?act prov:used ?code .
                    ?code a sageprov:DataHandlingCode .
                }}
                OPTIONAL {{
                    ?act prov:used ?dep .
                    ?dep a sageprov:ObtainedFile .
                    ?newerdep prov:wasRevisionOf ?dep .
                }}
            }}
        }}
        '''.format(
            sageprov_uri=URIRef(SAGEPROV).n3(),
            prov_uri=URIRef(PROV).n3()))
    uningested_query.setReturnFormat(JSON)
    uningested_results = uningested_query.query().convert()
    for r in uningested_results['results']['bindings']:
        df_uri = URIRef(r['df']['value'])
        if len(r['act']) > 0:
            # at least once, we started ingesting df_uri
            act_uri = URIRef(r['act']['value'])
            if len(r['fin']) > 0:
                # this ingest activity finished
                log.debug('%s was fully ingested by activity %s',
                          df_uri, act_uri)
                if len(r['code']) > 0:
                    if r['code']['value'] == str(data_version_uri):
                        log.debug(' - with this version of the code, %s', data_version_uri)
                        my_transforms = set(uri for (name, checksum, uri) in transforms())
                        these_transforms = set(transforms_used_by(act_uri, sparql_url))
                        if len(these_transforms) == 0:
                            log.debug(' - did not use any transforms. no XML files in zip?')
                        elif these_transforms.issubset(my_transforms):
                            log.debug(' - with transforms %r, a subset of our current '
                                      'transforms',
                                      these_transforms)
                            if len(r['newerdep']) > 0:
                                log.debug(' * using %s of which a newer '
                                          'version %s has been fetched',
                                          r['dep']['value'],
                                          r['newerdep']['value'])
                                yield (fti_used_old_downloaded_file, act_uri, df_uri)
                            else:
                                log.debug(' - using current downloaded files')
                                # put more conditions here
                        else:
                            log.debug(' * with transforms %r which are not a subset of '
                                      'our current transforms %r', these_transforms,
                                      my_transforms)
                            yield (fti_ingested_with_old_transforms, act_uri, df_uri)
                    else:
                        log.debug(' * with version %s which is not the '
                                  'current version %s',
                                  r['code']['value'], data_version_uri)
                        yield (fti_ingested_by_old_code, act_uri, df_uri)
                else:
                    # no one knows what version of the code did this
                    # ingest. let's say it's old.
                    log.debug(' * don\'t know what version of the code did it')
                    yield (fti_ingested_by_old_code, act_uri, df_uri)
            else:
                log.debug('%s was partially ingested', df_uri)
                yield (fti_partially_ingested, act_uri, df_uri)
        else:
            log.debug('%s never ingested before', df_uri)
            yield (fti_uningested, None, df_uri)


def entities_generated_by(act_uri, sparql_url):
    query = SPARQLWrapper(sparql_url)
    query.setQuery(
        '''
        PREFIX sageprov: {sageprov_uri}
        PREFIX prov: {prov_uri}
        SELECT DISTINCT ?g WHERE {{
            GRAPH ?g {{ }} .
            ?g a prov:Entity .
            ?g prov:wasGeneratedBy {act_uri} .
        }}
        '''.format(
            sageprov_uri=URIRef(SAGEPROV).n3(),
            prov_uri=URIRef(PROV).n3(),
            act_uri=act_uri.n3()))
    query.setReturnFormat(JSON)
    results = query.query().convert()
    for r in results['results']['bindings']:
        yield URIRef(r['g']['value'])


def transforms_used_by(act_uri, sparql_url):
    query = SPARQLWrapper(sparql_url)
    # We want to know what transforms were used because if the
    # transforms we now have are disjoint with the set of transforms
    # used, we should re-ingest. But the thing we are deciding whether
    # to reingest or not is the DownloadedFile, and each layer of
    # zip-files inside it causes a sub-ingest activity, and only when
    # we are actually ingesting an XML file do we use transforms. So,
    # if the ingest of the file we downloaded used some transforms, we
    # want to know about those. Or if the file we downloaded was a ZIP
    # file which contained an XML file, and when ingesting that XML
    # file we used some transforms, we want to know about those. It's
    # frequent that ZIPs contain ZIPs, which then contain XMLs. So we
    # need to look three ingests deep for transforms used.  SPARQL 1.1
    # supports querying for a chain of properties, but 4store only
    # supports SPARQL 1.0, so we have to explicitly lay things out.
    query.setQuery(
        '''
        PREFIX sageprov: {sageprov_uri}
        PREFIX prov: {prov_uri}
        SELECT DISTINCT ?e WHERE {{
            {{
              {act_uri} a prov:Activity .
              {act_uri} prov:used ?e .
              ?e a sageprov:Transform .
            }} UNION {{
              {act_uri} a prov:Activity .
              ?other_act prov:wasInformedBy {act_uri} .
              ?other_act prov:used ?e .
              ?e a sageprov:Transform .
            }} UNION {{
              {act_uri} a prov:Activity .
              ?other1_act prov:wasInformedBy {act_uri} .
              ?other2_act prov:wasInformedBy ?other1_act .
              ?other2_act prov:used ?e .
              ?e a sageprov:Transform .
            }}
        }}
        '''.format(
            sageprov_uri=URIRef(SAGEPROV).n3(),
            prov_uri=URIRef(PROV).n3(),
            act_uri=act_uri.n3()))
    query.setReturnFormat(JSON)
    results = query.query().convert()
    for r in results['results']['bindings']:
        yield URIRef(r['e']['value'])


def mime_type_of(downloaded_file_uri, sparql_url):
    mime_type_query = SPARQLWrapper(sparql_url)
    mime_type_query.setQuery(
        '''
        PREFIX sageprov: {sageprov_uri}
        PREFIX prov: {prov_uri}
        SELECT ?mimetype WHERE {{
            {downloaded_file_uri} a sageprov:ObtainedFile .
            {downloaded_file_uri} prov:wasGeneratedBy ?a .
            ?a a sageprov:Download .
            ?a prov:endedAtTime ?et .
            ?a sageprov:hadContentType ?mimetype .
        }}
        '''.format(
            sageprov_uri=URIRef(SAGEPROV).n3(),
            prov_uri=URIRef(PROV).n3(),
        downloaded_file_uri=downloaded_file_uri.n3()))
    mime_type_query.setReturnFormat(JSON)
    mime_type_results = mime_type_query.query().convert()
    for r in mime_type_results['results']['bindings']:
        return r['mimetype']['value']
    raise KeyError('MIME type not found for', downloaded_file_uri)


def filename_of(downloaded_file_uri, sparql_url):
    filename_query = SPARQLWrapper(sparql_url)
    filename_query.setQuery(
        '''
        PREFIX sageprov: {sageprov_uri}
        PREFIX prov: {prov_uri}
        SELECT ?filename WHERE {{
            {downloaded_file_uri} a sageprov:ObtainedFile .
            {downloaded_file_uri} sageprov:hadFilename ?filename .
        }}
        '''.format(
            sageprov_uri=URIRef(SAGEPROV).n3(),
            prov_uri=URIRef(PROV).n3(),
            downloaded_file_uri=downloaded_file_uri.n3()))
    filename_query.setReturnFormat(JSON)
    filename_results = filename_query.query().convert()
    for r in filename_results['results']['bindings']:
        return r['filename']['value']
    raise KeyError('Filename not found for', downloaded_file_uri)


URL_SAFE_ISO8601 = '%Y-%m-%dT%H%M%S,%f'
def _mint_ingest_activity_uri(now):
    return Xingest[now.strftime(URL_SAFE_ISO8601)]

def ingest(sparql_url, data_post_url):
    log = logging.getLogger('ingest')
    to_ingest = set([])
    for condition, activity_uri, downloaded_file_uri in files_to_ingest(sparql_url):
        log.debug(' * going to reingest %s because %r', downloaded_file_uri, condition)
        if activity_uri is not None:
            for ent_uri in entities_generated_by(activity_uri, sparql_url):
                delete_graph(data_post_url, ent_uri)
            delete_graph(data_post_url, activity_uri + '/graph')
        # we keep a set of the downloaded_file_uris because some may
        # have been ingested multiple times, but we only want to
        # reingest these once
        to_ingest.add((downloaded_file_uri, condition))
    for downloaded_file_uri, condition in to_ingest:
        log.info('reingesting %s because %r', downloaded_file_uri, condition)
    for downloaded_file_uri, condition in to_ingest:
        # now, ingest
        filename = filename_of(downloaded_file_uri, sparql_url)
        try:
            try:
                mime_type = mime_type_of(downloaded_file_uri, sparql_url)
                handle_file_by_mimetype(downloaded_file_uri, filename, 
                                        lambda: open(filename, 'rb'),
                                        mime_type, sparql_url, data_post_url)
            except KeyError:
                # this file is inducted, not downloaded
                handle_file_by_name(downloaded_file_uri, filename,
                                    lambda: open(filename, 'rb'),
                                    None, sparql_url, data_post_url)
        except NoHandler, e:
            log.error(repr(e))
        except NoDocument, e:
            log.error(repr(e))
        except KeyboardInterrupt:
            raise
        except:
            # this file failed. try further ones anyway.
            pass

        
def handle_file_by_mimetype(uri, filename, get_stream, mime_type, 
                            sparql_url, data_post_url):
    """Handle a downloaded file according to its Content-Type.

    If no handler is available for the Content-Type fetched, this is a
    problem: we have downloaded a file we don't know how to deal with.

    """
    try:
        handler = mime_handlers[mime_type]
    except KeyError:
        raise NoHandler('for mime type', mime_type)
    now = datetime.datetime.utcnow()
    iuri = _mint_ingest_activity_uri(now)
    pre = Graph()
    pre.add((iuri, RDF.type, PROV.Activity))
    pre.add((iuri, RDF.type, SAGEPROV.Ingest))
    pre.add((iuri, PROV.startedAtTime, Literal(now)))
    pre.add((iuri, PROV.used, data_version_uri))
    # Ingesting files we downloaded uses Entities we created.
    pre.add((iuri, PROV.used, uri))
    post_to_graph_turtle(data_post_url, iuri + '/graph', pre)
    handler(uri, filename, get_stream, iuri, sparql_url, data_post_url)
    now = datetime.datetime.utcnow()
    post = Graph()
    post.add((iuri, PROV.endedAtTime, Literal(now)))
    post_to_graph_turtle(data_post_url, iuri + '/graph', post)

def handle_file_by_name(uri, filename, get_stream, ingest_uri, 
                        sparql_url, data_post_url):
    """Handle a file inside a ZIP by its filename, which is all we have.

    If no handler is available for a file inside a ZIP, we can skip it
    with no problems.

    """
    log = logging.getLogger('handle_file_by_name')
    log.debug('handling file named %r', filename)
    for regex, handler in file_regexes:
        if re.search(regex, filename):
            now = datetime.datetime.utcnow()
            # ingest_uri (parameter) is the URI of the Activity to
            # ingest the zip file we are in. iuri (local variable) is
            # the URI of the Activity to ingest a file inside the zip.
            iuri = _mint_ingest_activity_uri(now)
            pre = Graph()
            pre.add((iuri, RDF.type, PROV.Activity))
            pre.add((iuri, RDF.type, SAGEPROV.Ingest))
            pre.add((iuri, PROV.startedAtTime, Literal(now)))
            if ingest_uri is None:
                # we are a top-level ingest, not subordinate to a
                # handle_file_by_mimetype
                pre.add((iuri, PROV.used, data_version_uri))
                # Ingesting files we downloaded uses Entities we created.
                pre.add((iuri, PROV.used, uri))
            else:
                # wasInformedBy implies that an Entity (the zip member)
                # passed between two Activities, without naming it or
                # discussing it further.
                pre.add((iuri, PROV.wasInformedBy, ingest_uri))
            post_to_graph_turtle(data_post_url, iuri + '/graph', pre)
            # Not done here, but could be possibly done: write triples
            # about the zip file as a prov:Collection.
            handler(uri, filename, get_stream, iuri,
                           sparql_url, data_post_url)
            now = datetime.datetime.utcnow()
            post = Graph()
            post.add((iuri, PROV.endedAtTime, Literal(now)))
            post_to_graph_turtle(data_post_url, iuri + '/graph', post)
            break
    else:
        pass



def handle_zip_file(uri, filename, get_stream, ingest_uri, 
                    sparql_url, data_post_url):
    """Handle a zip file by going through filenames inside and handling them.

    Zip files may well be inside zip files. The ZipExtFile returned
    by ZipFile.open cannot seek, but the ZipFile constructor tries
    immediately to seek to the end of its input stream. So to open a
    zip file inside a zip file, we must read the inner zip out and put
    it somewhere seekable: either a temporary file or a BytesIO.

    In 2014, I tried to characterize the ZIP files we'll be operating
    on, and they are sized like so -

    =============== ==============
    Figure          Value in bytes
    =============== ==============
    Minimum         23000
    First quartile  332999
    Median          707847
    Third quartile  1099171
    Maximum         9223915
    =============== ==============

    There were 158 zip files. Each of these contains one or more zip
    files, but each zip can't be any larger than the zip it's inside
    of.

    I expect hundreds of megabytes of RAM to be free on any host this
    runs on. So BytesIO it is.

    """
    log = logging.getLogger('handle_zip_file')
    log.debug('a zip! found in %s, filename %s', uri, filename)
    zipstream = ZipFile(get_stream())
    for zipmembername in zipstream.namelist():
        try:
            handle_file_by_name(uri, zipmembername,
                                lambda: io.BytesIO(zipstream.read(zipmembername)),
                                ingest_uri,
                                sparql_url, data_post_url)
        except NoHandler, e:
            log.error(repr(e))


def handle_xml_file(uri, filename, get_stream, ingest_uri, 
                    sparql_url, data_post_url):
    log = logging.getLogger('handle_xml_stream')
    log.info('an xml! found in %s, filename %s', uri, filename)
    # Possible optimization: use SAX; make it quit as soon as we get
    # the event for the beginning root element tag.
    thetree = etparse(get_stream())
    root = thetree.getroot()
    log.debug('root element is %r', root.tag)
    try:
        handler = xml_root_element_handlers[root.tag]
    except KeyError:
        raise NoHandler('for xml file with root tag', root.tag)
    # NOTE: the handler will likely call get_stream again, so it
    # should work multiple times.
    handler(uri, filename, get_stream, thetree, ingest_uri,
            sparql_url, data_post_url)
    log.debug('Ingest %s XML part complete' % ingest_uri)


def latest_of_class(class_uriref, description, sparql_url):
    query = SPARQLWrapper(sparql_url)
    query.setQuery(
        '''
        PREFIX sageprov: {sageprov_uri}
        PREFIX prov: {prov_uri}
        SELECT ?sp WHERE {{
            ?sp a {class_uri} .
            ?sp prov:wasGeneratedBy ?act .
            ?act prov:endedAtTime ?t .
        }} ORDER BY DESC(?t) LIMIT 1
        '''.format(
            sageprov_uri=URIRef(SAGEPROV).n3(),
            prov_uri=URIRef(PROV).n3(),
            class_uri=class_uriref.n3()))
    query.setReturnFormat(JSON)
    results = query.query().convert()
    for r in results['results']['bindings']:
        return URIRef(r['sp']['value'])
    raise NoDocument(description)

latest_sp80053_list = partial(latest_of_class,
                              URIRef(SP80053LOD.NistSP80053),
                              'SP 800-53 controls XML')
latest_cci_list = partial(latest_of_class,
                          URIRef(CCILOD.CCIList),
                          'CCI list')


xslt_functions = FunctionNamespace('http://securityrules.info/ns/xslt/sagemincer/ccilist')
def find_sp80053v4_requirement(context, sp80053v4_uri, index):
    try:
        return sagemincer.find_requirement.find(sp80053v4_uri, index)
    except KeyError:
        return ''
xslt_functions['find_sp80053v4_requirement'] = find_sp80053v4_requirement

def handle_xml_cci_list(uri, filename, get_stream, thetree, ingest_uri, 
                        sparql_url, data_post_url):
    log = logging.getLogger('handle_xml_cci_list')
    log.debug('handling')
    sp80053v4_uri = latest_sp80053_list(sparql_url)
    transform_stream, transform_uri = get_transform('from-cci-list.xsl')
    transform = XSLT(etparse(transform_stream))
    pre = Graph()
    pre.add((ingest_uri, PROV.used, transform_uri))
    pre.add((ingest_uri, PROV.used, sp80053v4_uri))
    post_to_graph_turtle(data_post_url, ingest_uri + '/graph', pre)
    checksum = shorten_hash(raw_hash(get_stream().read()))
    document_uri = Xid[checksum]
    document_pre = Graph()
    document_pre.add((document_uri, RDF.type, CCILOD.CCIList))
    document_pre.add((document_uri, RDF.type, SAGEPROV.RequiringDocument))
    document_pre.add((document_uri, RDF.type, PROV.Entity))
    document_pre.add((document_uri, PROV.wasGeneratedBy, ingest_uri))
    document_pre.add((document_uri, PROV.wasDerivedFrom, uri))
    delete_graph(data_post_url, document_uri)
    post_to_graph_turtle(data_post_url, document_uri, document_pre)
    rdfxml = transform(thetree, 
                       nist_sp_800_53_v4_uri=XSLT.strparam(str(sp80053v4_uri)),
                       cci_list_uri=XSLT.strparam(str(document_uri)))
    post_to_graph_rdfxml(data_post_url, document_uri, rdfxml)
    log.debug('CCI list specific part complete for ingest %s', ingest_uri)


def handle_xml_sp80053_list(uri, filename, get_stream, thetree, ingest_uri, 
                            sparql_url, data_post_url):
    log = logging.getLogger('handle_xml_sp80053_list')
    log.debug('handling')
    transform_stream, transform_uri = get_transform('from-800-53-xml.xsl')
    transform = XSLT(etparse(transform_stream))
    pre = Graph()
    pre.add((ingest_uri, PROV.used, transform_uri))
    post_to_graph_turtle(data_post_url, ingest_uri + '/graph', pre)
    checksum = shorten_hash(raw_hash(get_stream().read()))
    document_uri = Xid[checksum]
    document_pre = Graph()
    document_pre.add((document_uri, RDF.type, SP80053LOD.NistSP80053))
    document_pre.add((document_uri, RDF.type, SAGEPROV.RequiringDocument))
    document_pre.add((document_uri, RDF.type, PROV.Entity))
    document_pre.add((document_uri, PROV.wasGeneratedBy, ingest_uri))
    document_pre.add((document_uri, PROV.wasDerivedFrom, uri))
    delete_graph(data_post_url, document_uri)
    post_to_graph_turtle(data_post_url, document_uri, document_pre)
    rdfxml = transform(thetree,
                       nist_sp_800_53_v4_document_uri=XSLT.strparam(str(document_uri)))
    post_to_graph_rdfxml(data_post_url, document_uri, rdfxml)
    log.debug('NIST SP 800-53 specific part complete for ingest %s', ingest_uri)


def handle_xml_xccdf_checklist(uri, filename, get_stream, thetree, ingest_uri, 
                               sparql_url, data_post_url):
    log = logging.getLogger('handle_xml_xccdf_checklist')
    log.debug('handling')
    cci_uri = latest_cci_list(sparql_url)
    transform1_stream, transform1_uri = get_transform('from-xccdf-1.1.xsl')
    transform1 = XSLT(etparse(transform1_stream))
    # FIXME: by using this transform without question, we are assuming
    # that all XCCDF checklists are STIGs. While this is borne out by
    # present usage, it may stop being true in the future.
    transform2_stream, transform2_uri = get_transform('from-stig.xsl')
    transform2 = XSLT(etparse(transform2_stream))
    pre = Graph()
    pre.add((ingest_uri, PROV.used, transform1_uri))
    pre.add((ingest_uri, PROV.used, transform2_uri))
    pre.add((ingest_uri, PROV.used, cci_uri))
    post_to_graph_turtle(data_post_url, ingest_uri + '/graph', pre)
    checksum = shorten_hash(raw_hash(get_stream().read()))
    document_uri = Xid[checksum]
    document_pre = Graph()
    document_pre.add((document_uri, RDF.type, XCCDFLOD.Checklist))
    document_pre.add((document_uri, RDF.type, SAGEPROV.RequiringDocument))
    document_pre.add((document_uri, RDF.type, PROV.Entity))
    document_pre.add((document_uri, PROV.wasGeneratedBy, ingest_uri))
    document_pre.add((document_uri, PROV.wasDerivedFrom, uri))
    delete_graph(data_post_url, document_uri)
    post_to_graph_turtle(data_post_url, document_uri, document_pre)
    for tx in (transform1, transform2):
        rdfxml = tx(thetree, 
                    stig_uri=XSLT.strparam(str(document_uri)),
                    cci_base_uri=XSLT.strparam(str(cci_uri)+'/'))
        post_to_graph_rdfxml(data_post_url, document_uri, rdfxml)
    log.debug('XCCDF STIG specific part complete for ingest %s', ingest_uri)


mime_handlers = {
    'application/zip': handle_zip_file,
    # HAAAAACK. But why did they quit saying the right MIME type??
    'binary/octet-stream': handle_zip_file,
    'application/x-zip-compressed': handle_zip_file,
    'text/xml': handle_xml_file,
}

file_regexes = (
    (r'\.zip$', handle_zip_file),
    (r'\.xml$', handle_xml_file),
)

xml_root_element_handlers = {
    '{http://iase.disa.mil/cci}cci_list': handle_xml_cci_list,
    '{http://scap.nist.gov/schema/sp800-53/feed/2.0}controls': handle_xml_sp80053_list,
    '{http://checklists.nist.gov/xccdf/1.1}Benchmark': handle_xml_xccdf_checklist,
}
