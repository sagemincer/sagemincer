# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from sagemincer.url_join import url_path_join
import urllib

class TestUrlJoin(unittest.TestCase):
    def test_add_relative(self):
        """from http://codereview.stackexchange.com/questions/13027"""
        self.assertEqual(url_path_join('https://example.org/fizz', 'buzz'),
                         'https://example.org/fizz/buzz')

    def test_pick_scheme(self):
        """from http://codereview.stackexchange.com/questions/13027"""
        self.assertEqual(url_path_join('https://', 'http://www.example.org',
                                       '?fuzz=buzz'),
                         'https://www.example.org?fuzz=buzz')

    def test_preserve_trailing_slash(self):
        """url_path_join should not eat trailing slashes:"""
        self.assertEqual(url_path_join('http://example.org/foo',
                                       urllib.quote('http://example.org/bar/')),
                'http://example.org/foo/http%3A//example.org/bar/')

if __name__ == '__main__':
    unittest.main()
