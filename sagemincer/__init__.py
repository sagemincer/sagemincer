# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from rdflib.namespace import Namespace
from hashlib import sha256
from base64 import urlsafe_b64encode, urlsafe_b64decode
from sagemincer.environment import resource_root_uri
from sagemincer.bubblebabble import babble
from bottle import HTTPError
import re

user_agent_string = 'sagemincer/0.3 +https://gitlab.com/sagemincer/sagemincer/'

# Bump this version when changes are made to the code that impact the
# data. For example, if a change in the code adds new provenance data,
# increment this number. It's a decimal integer for simplicity of
# maintenance, but does not carry any explicit semantic meaning: that
# is, you shouldn't hack up two URIs containing values of this number
# and compare the integers in order to make some conclusion. Instead,
# follow the chain of prov:wasRevisionOf properties.
DATA_VERSION = '17'

# This namespace is used to qualify classes and predicates. It is fixed.
# It is also written in the XSL stylesheets.
XCCDFLOD  = Namespace(u'http://securityrules.info/ns/xccdflod/1#')
STIGLOD   = Namespace(u'http://securityrules.info/ns/stiglod/1#')
SP80053LOD = Namespace(u'http://securityrules.info/ns/sp80053lod/1#')
CCILOD    = Namespace(u'http://securityrules.info/ns/ccilod/1#')
SAGEPROV = Namespace(u'http://securityrules.info/ns/sageprov/1#')

# These namespaces are used in constructing URIs for resources like checklists
# or checklist items, not resources from a vocabulary or ontology. The
# resulting URIs will be taken straight from the RDF graph and put into HTML
# documents as link destinations. So they must point inside the namespace where
# this application is running, and they may vary between running instances of
# this application.
Xid       = Namespace(resource_root_uri + u'/id/')
Xdownload = Namespace(resource_root_uri + u'/id/dl/')
Xinduct   = Namespace(resource_root_uri + u'/id/in/')
Xingest   = Namespace(resource_root_uri + u'/id/ingest/')

# This one contains data about things inside the sagemincer codebase
Xsm       = Namespace(resource_root_uri + u'/id/sagemincer/')
Xsmv      = Namespace(resource_root_uri + u'/id/sagemincer/code/')
data_version_uri = Xsmv[DATA_VERSION]

PROV = Namespace(u'http://www.w3.org/ns/prov#')

hash_property = SAGEPROV.hadSHA256
hash_algorithm = sha256
def raw_hash(data):
    return hash_algorithm(data).digest()


def safen_hash(digest):
    """Encode an entire digest value so it can be safely used in filenames."""
    return urlsafe_b64encode(digest)

def shorten_hash(digest):
    """Shorten a digest value to be human-readable and reasonably unique.

    This used to be the first ten characters of the base 64 encoding of the
    digest value. That's 60 bits, where people generally identify Git
    changesets with at most 32 bits. Now it's 56 bits, without the ambiguities
    between 1 and l or 0 and O (etc) that base 64 encoding had.
    """
    return babble(digest[:7])

def shorten_safe_hash(safe_digest):
    """Shorten a safened digest value (see shorten_hash)."""
    unsafe = urlsafe_b64decode(safe_digest)
    return shorten_hash(unsafe)

def to_current_hash_format(doc_hash):
    if re.match(r'^x([a-z]{4}-[a-z]){3}[a-z]{3}x$', doc_hash):
        return doc_hash
    elif re.match(r'^[a-zA-Z0-9_-]{10}$', doc_hash):
        # our old base64-encoded slice of a digest value was 60 bits long, and we only need 56
        the_bytes = urlsafe_b64decode(doc_hash + '==')
        return shorten_hash(the_bytes)
    else:
        raise HTTPError(404, "Document ID not understood")
