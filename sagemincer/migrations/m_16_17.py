import sys
import logging
from sagemincer import (
    PROV, SAGEPROV, Xdownload, Xingest, Xid, SP80053LOD, CCILOD, XCCDFLOD)
from rdflib.term import URIRef
from sagemincer.environment import sparql_url, data_post_url
from sagemincer.fs_sparql import SPARQLWrapper, JSON
from rdflib import Graph, RDF
from sagemincer.to_4store import post_to_graph_turtle

def add_superclass(existing_class_uri, new_class_uri):
    log = logging.getLogger('migrate')
    q = SPARQLWrapper(sparql_url)
    q.setQuery('''
        SELECT DISTINCT ?g ?s WHERE {{
            GRAPH ?g {{
                 ?s a {ecuri}
            }} }}'''.format(
                ecuri=existing_class_uri.n3()))
    q.setReturnFormat(JSON)
    for bs in q.query().convert()['results']['bindings']:
        s_uri = URIRef(bs['s']['value'])
        g_uri = URIRef(bs['g']['value'])
        q2 = SPARQLWrapper(sparql_url)
        q2.setQuery('''
            ASK {{ GRAPH {g_uri} {{
                      {s_uri} a {ncuri} }} }}'''.format(
                          g_uri=g_uri.n3(),
                          s_uri=s_uri.n3(),
                          ncuri=new_class_uri.n3()))
        q2.setReturnFormat(JSON)
        s_isa_new_class = q2.query().convert()['boolean']
        if not s_isa_new_class:
            g = Graph()
            g.add((s_uri, RDF.type, new_class_uri))
            log.info('%s\n', g.serialize(format='turtle'))
            post_to_graph_turtle(data_post_url, str(g_uri), g)


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
    add_superclass(SAGEPROV.DownloadedFile, SAGEPROV.ObtainedFile)
    add_superclass(SAGEPROV.Download, SAGEPROV.ObtainContent)
