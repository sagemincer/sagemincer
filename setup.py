#!/usr/bin/env python2.7
# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup, find_packages

# http://www.scotttorborg.com/python-packaging/metadata.html
def readme():
    with open('README.rst') as f:
            return f.read()


setup(name='sagemincer',
    # The version number is also written in sagemincer/__init__.py in the
    # user_agent_string
    version='0.4',
    description='Exposes XCCDF checklists as Linked Open Data',
    long_description=readme(),
    author='Jared Jennings',
    author_email='jjennings@fastmail.fm',
    license='AGPLv3+',
    packages=find_packages(),
    package_data={
        'sagemincer': ['transforms/*.xsl', 'ontologies/*'],
        'sagemincer.bottles': ['static/*', 'templates/*.html',
                               'templates/xccdflod/*.html',
                               'templates/xccdflod/checklist/*.html',
                               'templates/xccdflod/group/*.html',
                               'templates/xccdflod/profile/*.html',
                               'templates/xccdflod/rule/*.html',
                               'templates/ccilod/*.html',
                               'templates/ccilod/ccilist/*.html',
                               'templates/ccilod/item/*.html',
                               'templates/sp80053lod/*.html',
                               'templates/sp80053lod/control/*.html',
                               'templates/sp80053lod/enhancement/*.html',
                               'templates/sp80053lod/nistsp80053/*.html',
                               'templates/sp80053lod/statement/*.html',
                               ],
    },
    include_package_data=True,
    url='https://gitlab.com/sagemincer/sagemincer/',
    install_requires=[
        'rdflib',
        'mock',
        'bottle',
        'mako',
        'mimerender',
        'lxml',
        'pyxdg',
        'isodate',
        'requests',
        'logutils',
    ],
    # We require FuXi from GitHub because it supports RDFLib 4
    dependency_links=[
        'git+https://github.com/RDFLib/FuXi.git@d4342c74036c0d26e03d11855f4a51dd0152c5cf#egg=FuXi-1.4.dev',
    ],
    classifiers=[
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: System Administrators',
        'Natural Language :: English',
        'Operating System :: POSIX :: Linux',
        'Topic :: Security',
    ],
)
