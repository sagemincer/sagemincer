# sagemincer - expose security checklists as Linked Open Data
# Copyright (C) 2014 Jared Jennings <jjennings@fastmail.fm>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM centos:centos7
RUN yum -y localinstall https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN yum -y install python python-beaker python-pip python-lxml
# This is written here so Docker can cache the installation of all these things
# sagemincer depends upon, so that if you are developing sagemincer and
# repeatedly running docker build, it will happen faster.
RUN pip install wheel rdflib mock bottle mimerender pyxdg six isodate pyparsing SPARQLWrapper html5lib python-mimeparse
EXPOSE 8081
ENV SAGEMINCER_RESOURCE_ROOT_URI http://127.0.0.1:8081
ADD . /tmp/sagemincer-source
WORKDIR /tmp/sagemincer-source
RUN pip install .
# Run with the command "python -m sagemincer.fetch" to fill the triple store.
# The fetcher caches its downloads in /.cache. Maybe you want a volume for
# that.
#
# When running, you should --link my_4store_container:fourstore
CMD python -m sagemincer.serve.trivial
