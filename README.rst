What's it do?
-------------

sagemincer provides, to people and to scripts, a way to refer to single
security requirements from anywhere, and get information about them.

It does this by exposing XCCDF data using the Linked Open Data paradigm.

There is an instance of this software running at
`securityrules.info <http://securityrules.info>`_.


XCCDF
-----

The eXtensible Configuration Checklist Description Format (XCCDF) is
an XML format for describing how a system should be configured at a
high level. It's defined by the U.S. `National Institute of Standards
and Technology (NIST) Interagency Report (IR) 7275
<http://scap.nist.gov/specifications/xccdf/>`_ and is part of the
`Security Content Automation Protocol (SCAP) suite
<http://scap.nist.gov/>`_.


XCCDF data
----------

NIST runs `the National Checklist Program (NCP)
<http://web.nvd.nist.gov/view/ncp/repository>`_, which contains XCCDF
content.

Some good portion of that content is provided by the U.S. Department
of Defense (DoD), which makes Security Technical Implementation Guides
(STIGs) available not only to the NCP but also on `their own page
<http://iase.disa.mil/stigs>`_. Most STIGs are written as XCCDF
checklists.

sagemincer obtains and digests the ZIP files from this site which contain XCCDF
content.


Linked Open Data
----------------

`Linked open data <http://linkeddata.org/>`_ "is about using the Web
to connect related data that wasn't previously linked... using URIs
and RDF." With it, data, and links between data, can be freed from
being locked inside one document or database. There's so much written
about it elsewhere by others that there isn't any point in further
poetics about it here.

We transpose the elements defined in XCCDF to classes in RDFS, and the
content expressed in XCCDF documents into subject-predicate-object
triples in RDF, so to make each rule in each exposed XCCDF document
visible and linkable-to.


Exposing
--------

Each XCCDF document is given a Uniform Resource Identifier (URI) based
on a cryptographic checksum of its contents. Each Benchmark, Profile,
Rule and Group written in the document is given a URI based on the
checklist document's URI. All of these URIs are inside a namespace that's
configurable, so that they can be made fetchable as URLs. We provide the
data in various formats depending on what the fetcher of the URL asks
for. And the triples form links between the items exposed. This
fulfills the `four expectations of Linked Data
<http://www.w3.org/DesignIssues/LinkedData.html>`_, enabling "the
unexpected re-use of information which is the value added by the web."


To run your own
---------------

You need 4store. On Debian, you can apt-get install it. Otherwise, get it from
https://github.com/garlik/4store/archive/master.zip, unzip, configure, make,
sudo make install. See if you can find a configure switch, or write a
configuration file, or run it with a command-line switch, to make it use port
9000. See also http://4store.org.

You need a bunch of Python modules. Probably make a new virtual environment
with venv, activate it, cd into this directory, and ::

    pip install .

You should make all the links point to your own machine::

    export SAGEMINCER_RESOURCE_ROOT_URI=http://127.0.0.1:8081

Then you can fetch and process the data::

    python -m sagemincer.multifetch

Then start the server::

    python -m sagemincer.serve.debug

And visit the site in your browser at http://127.0.0.1:8081/.


Thanks
------

`RESTful Web Services with Python <http://www.slideshare.net/juokaz/restful-web-services-with-python-dynamic-languages-conference>`_
`Cool URIs for the Semantic Web <http://www.w3.org/TR/cooluris>`_


To work on sagemincer
---------------------

Issues are tracked at https://trello.com/b/Gl4UJwS6/sagemincer.
